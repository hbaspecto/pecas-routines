pecas-routines
==================================================

Routines used by the `PECAS <https://www.hbaspecto.com/products/pecas>`_ run scripts and tools.

This repository includes extensions to the PECAS model that can affect model results,
such as technology scaling, AA-SD price and space adjustments, and skim processing.
Therefore, once a scenario has been run with a given version of pecas-routines,
it should NOT be updated to a later version, as this could make the run not reproduceable.

The flagship module in this repository is ``pecas_routines``, which contains a
haphazard miscellany of functions used by PECAS models. We've slowly been
moving related groups of functions into their own modules, often moving
them to the ``hbautil`` repository
(e.g. the ``classpath``, ``mapit``, and ``pecassetup`` modules were created this way).
But there's still more work to be done on this front.

Licensed under the `Apache License Version 2.0 <https://www.apache.org/licenses/LICENSE-2.0>`_, see NOTICE.txt file.

Requires `hba-util <https://bitbucket.org/hbaspecto/hba-util>`_.


Links:

- Repo: https://bitbucket.org/hbaspecto/pecas-routines
- Licence: http://www.hbaspecto.com/products/pecas/software
- Docs: http://docs.office.hbaspecto.com/pecas-routines
