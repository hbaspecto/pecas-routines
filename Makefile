#
# pecas-routines/Makefile ---
#

SHELL:=/bin/bash
.SUFFIXES:

_default: _test

#####

# To install and test this in a scenario:
#
#   cd P298
#   source hba-setup.env
#   source ./ve/bin/activate
#
#   mkdir code
#   cd code
#   git clone git@bitbucket.org:hbaspecto/pecas-routines.git
#   cd pecas-routines
#   pip install --editable .
#
# Now you can edit and run this code.

#####

_dos2unix:
	dos2unix test_pecas_routines/fixtures/*

autopep8_files+=./pecas_routines/pecas_apply_optionchanges.py

_autopep8:
	autopep8 -a -i ${autopep8_files}

#####

_test:
	cd ./test_pecas_routines && make _test
