import csv
import unittest
from os.path import join

import hbautil.scriptutil as su
import pecas_routines as pr
import pecas_routines.employment as emp
from pecas_test import mockscen
from pecas_test import mocksettings

ps = mocksettings.PecasSettings().with_machine_settings().with_test_sd().clone_with(
    sd_schema="test",
    baseyear=2011,
    scendir=join("test", "Employment"),
    base_employment_fname="BaseEmployment.csv",
    employment_use_work_at_home=False
)


class TestEmployment(unittest.TestCase):
    output_fname = join(ps.scendir, "2039", "Employment.csv")
    expected_fname = join(ps.scendir, "JohnExampleResult.csv")
    wah_expected_fname = join(ps.scendir, "JohnExampleWahResult.csv")

    @classmethod
    def setUpClass(cls):
        mockscen.create_database(ps)

    def setUp(self):
        mockscen.create_schema(ps)

    def testSimpleExample(self):
        emp.main(ps, pr.connect_to_sd_fn(ps), 2039)
        self.assertEmploymentFilesEqual(self.output_fname, self.expected_fname)
        su.backup(self.output_fname, "Simple")

    def testWorkAtHome(self):
        wah_ps = ps.clone_with(employment_use_work_at_home=True)
        emp.main(wah_ps, pr.connect_to_sd_fn(ps), 2039)
        self.assertEmploymentFilesEqual(self.output_fname, self.wah_expected_fname)
        su.backup(self.output_fname, "Wah")

    def assertEmploymentFilesEqual(self, actual, expected):
        with open(actual) as actf:
            act_reader = csv.reader(actf)
            next(act_reader)

            with open(expected) as expf:
                exp_reader = csv.reader(expf)
                next(exp_reader)

                for line, (act_line, exp_line) in enumerate(zip(act_reader, exp_reader)):
                    act_taz, act_naics, act_noc, act_amt = act_line
                    exp_taz, exp_naics, exp_noc, exp_amt = exp_line
                    self.assertEqual(exp_taz, act_taz,
                                     "TAZs not equal on line {}: {} vs {}".format(line, exp_taz, act_taz))
                    self.assertEqual(exp_naics, act_naics,
                                     "NAICS not equal on line {}: {} vs {}".format(line, exp_naics, act_naics))
                    self.assertEqual(exp_noc, act_noc,
                                     "NOCs not equal on line {}: {} vs {}".format(line, exp_noc, act_noc))
                    act_amt, exp_amt = float(act_amt), float(exp_amt)
                    self.assertAlmostEqual(act_amt, exp_amt, delta=0.001,
                                           msg="Values not equal at TAZ {}, NAICS {}, NOC {}: {} vs {}".format(
                                               exp_taz, exp_naics, exp_noc, exp_amt, act_amt
                                           ))
