import unittest
from os.path import join

import hbautil.scriptutil as su
import pecas_routines as pr
from pecas_test import mocksettings
from pecas_test import mockscen
from test_pecas_routines import pr_mockscen

ps = mocksettings.PecasSettings().with_machine_settings().with_test_sd().clone_with(
    parcels_table="parcels",
    scendir=join("test", "mapit"),
)


class TestSpaceTazLimits(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        mockscen.create_database(ps)

    def setUp(self):
        self.standard_ps = ps
        mockscen.set_up_tables(self.standard_ps)

    def testUpdateSpaceLimits(self):
        pr_mockscen.create_simple_mockscen_2_9(self.standard_ps)
        pr_mockscen.add_space_taz_limits(self.standard_ps)
        self.add_existing_space_taz_limits()
        self.create_space_taz_limits_generator()

        def once_thru():
            exp_table = [
                [3, 54, None, 2000, 2017, True],
                [2, 53, 200, 1500, 2019, True]
            ]

            pr.update_space_limits(self.standard_ps, 2017)
            exp_table += [
                [1, 51, None, 1463, 2017, False],
                [2, 51, None, 250, 2017, False],
                [3, 53, None, 1000, 2017, False],
                [2, 54, 3000, None, 2017, False],
                [2, 53, 40, 1600, 2017, False]
            ]
            self.assertTableIs("space_taz_limits", exp_table)

            self.do2017Construction()
            pr.update_space_limits(self.standard_ps, 2018)
            exp_table += [
                [1, 51, None, 2013, 2018, False],
                [2, 51, None, 2365, 2018, False],
                [3, 53, None, 1100, 2018, False],
                [2, 54, 3300, None, 2018, False],
                [3, 54, 5000, 6000, 2018, False],
                [2, 53, 80, 1100, 2018, False]
            ]
            self.assertTableIs("space_taz_limits", exp_table)

            self.do2018Construction()
            self.do2019Construction()
            pr.update_space_limits(self.standard_ps, 2020)
            exp_table += [
                [1, 51, None, 2948, 2020, False],
                [2, 51, None, 2365, 2020, False],
                [3, 53, None, 1331, 2020, False],
                [2, 54, 5000, None, 2020, False],
                [3, 54, 6050, 7000, 2020, False]
            ]
            self.assertTableIs("space_taz_limits", exp_table)

        once_thru()
        pr_mockscen.create_simple_mockscen_2_9(self.standard_ps, reset=True)
        once_thru()

    def add_existing_space_taz_limits(self):
        with self.transaction() as tr:
            tr.query(
                "insert into {sch}.space_taz_limits\n"
                "values\n"
                # Remnant of previous automatic generation should be removed
                "(1, 53, null, 1000, 2017, false),\n"
                # Manual limit should be respected until overridden by a later automatic rule
                "(3, 54, null, 2000, 2017, true),\n"
                # Manual limit should override previous automatic rules
                "(2, 53, 200, 1500, 2019, true)")

    def create_space_taz_limits_generator(self):
        # The columns in the space_taz_limits_generator are:
        #   taz_group_id - The TAZ group
        #   taz_limit_group_id - The space type group
        #   min_quantity - Minimum space in the initial effective year
        #           (if omitted, there is no minimum)
        #   max_quantity - Maximum space in the initial effective year
        #           (if omitted, there is no maximum)
        #   percent_annual_change_to_min - Minimum (if present) is increased by
        #           this percentage each successive year
        #   percent_annual_change_to_max - If maximum present, it is increased
        #           by this percentage each year; otherwise, each year's maximum
        #           is this percentage above the actual total space from the
        #           previous year
        #   min_increment - If maximum is absent, then each year's maximum must
        #           exceed the previous year's total by at least this much
        #   year_effective - Initial effective year
        with self.transaction() as tr:
            tr.query(
                "create table {sch}.space_taz_limits_generator\n"
                "(\n"
                "   taz_group_id integer,\n"
                "   taz_limit_group_id integer,\n"
                "   year_effective integer,\n"
                "   min_quantity double precision,\n"
                "   max_quantity double precision,\n"
                "   percent_annual_change_to_min double precision,\n"
                "   percent_annual_change_to_max double precision,\n"
                "   min_increment double precision,\n"
                "   primary key (taz_group_id, taz_limit_group_id, year_effective)\n"
                ")")

            tr.query(
                "insert into {sch}.space_taz_limits_generator\n"
                "values\n"
                # Using the percent change to max
                "(1, 51, 2017, null, null, null, 10, null),\n"
                # Using the percent change to max and the min increment
                "(2, 51, 2017, null, null, null, 10, 100),\n"
                # Using a fixed 10% increase from an initial maximum
                "(3, 53, 2017, null, 1000, null, 10, null),\n"
                # Using a fixed 10% increase from an initian minimum
                "(2, 54, 2017, 3000, null, 10, null, null),\n"
                # Doesn't start until the second year
                "(3, 54, 2018, 5000, null, 10, null, 1000),\n"
                # Doesn't start until after the entire run (so no effect)
                "(3, 51, 2021, 9999, 9999, 9999, 9999, 9999),\n"
                # Starts before the model run, but should still compound
                "(2, 53, 2015, 10, null, 100, null, 100),\n"
                # Should override the previously specified rule in 2020
                "(2, 54, 2020, 5000, null, 10, null, null)")

    def do2017Construction(self):
        self.build(2017,
                   ["add", 1101, 80],
                   ["build", 2101, 2, 500],
                   ["build", 2201, 2, 2000]
                   )

    def do2018Construction(self):
        self.build(2018,
                   ["add", 2102, 1000]
                   )

    def do2019Construction(self):
        self.build(2019,
                   ["build", 1102, 3, 300],
                   ["add", 2301, 1000]
                   )

    def build(self, year, *events):
        with self.transaction(yr=year) as tr:
            for event in events:
                if event[0] == "add":
                    tr.query(
                        "update {sch}.parcels\n"
                        "set space_quantity = space_quantity + %(amt)s,\n"
                        "year_built = (space_quantity * year_built + "
                        "%(amt)s * %(yr)s) / (space_quantity + %(amt)s)\n"
                        "where pecas_parcel_num = %(par)s",
                        par=event[1], amt=event[2])
                elif event[0] == "build":
                    tr.query(
                        "update {sch}.parcels\n"
                        "set space_type_id = %(st)s,\n"
                        "space_quantity = %(amt)s,\n"
                        "year_built = %(yr)s\n"
                        "where pecas_parcel_num = %(par)s",
                        par=event[1], st=event[2], amt=event[3])
                else:
                    self.fail("Invalid event type {!r}".format(event[0]))

    def assertTableIs(self, table, values):
        with self.transaction(tbl=table) as tr:
            result = sorted(
                tr.query("select * from {sch}.{tbl}"), key=none_to_inf_key)
            # Make sure Nones reliably sort to the end

        values = sorted(values, key=none_to_inf_key)

        if len(values) != len(result):
            self.fail("Expected {} {}, but got {}".format(
                len(values), su.pl(len(values), "row"), len(result)))

        for result_row, values_row in zip(result, values):
            self.assertEqual(list(values_row), list(result_row))

    def transaction(self, *args, **kwargs):
        return pr.sd_querier(self.standard_ps).transaction(*args, **kwargs)


def none_to_inf_key(row):
    return [float("inf") if x is None else x for x in row]
