import pecas_routines as pr
import pecas_routines.migratesd2_9 as mig


def create_simple_mockscen_2_9(ps, reset=False):
    with pr.sd_querier(ps).transaction() as tr:
        if reset:
            tr.query("truncate table {sch}.parcels\n")
        else:
            tr.query(
                "insert into {sch}.luzs\n"
                "values\n"
                "(1, 'Foo'),\n"
                "(2, 'Bar')")

            tr.query(
                "insert into {sch}.tazs\n"
                "values\n"
                "(11, 1, 11),\n"
                "(12, 1, 12),\n"
                "(21, 2, 21),\n"
                "(22, 2, 22),\n"
                "(23, 2, 23)")

            tr.query(
                "insert into {sch}.space_types_group\n"
                "values\n"
                "(1, 'Residential', 1),\n"
                "(2, 'Commercial', 1)")

            params1 = "0.1, " + ("0, " * 10) + ("0.5, " * 7) + "0, "
            params2 = "0, 1, 1, 0, 0, 0, 0, "

            def st_record(id, name, code, group, min_d, max_d, end=False):
                result = ("({}, '{}', '{}', " + params1 + "{}, " + params2 + "{}, {})"
                          ).format(id, name, code, group, min_d, max_d)
                if not end:
                    result += ",\n"
                return result

            tr.query(
                "insert into {sch}.space_types_i\n"
                "values\n" +
                st_record(1, "Single family", "RSF", 1, 0, 0.2) +
                st_record(2, "Multifamily", "RMF", 1, 0, 5) +
                st_record(3, "Retail", "CRT", 2, 0, 0.5) +
                st_record(4, "Office", "COF", 2, 0, 5, end=True))

        tr.query(
            "insert into {sch}.parcels\n"
            "values\n"
            "('1101', 1101, 1980, 11, 1, 100, 1000, 1, false, false),\n"
            "('1102', 1102, 1999, 11, 1, 150, 750, 1, false, false),\n"
            "('1201', 1201, 2004, 12, 1, 150, 1000, 1, false, false),\n"
            "('1202', 1202, 1987, 12, 3, 1000, 2000, 1, false, false),\n"
            "('2101', 2101, 1964, 21, 1, 80, 600, 1, false, false),\n"
            "('2102', 2102, 2000, 21, 2, 1000, 1000, 1, false, false),\n"
            "('2201', 2201, 1972, 22, 3, 500, 1200, 1, false, false),\n"
            "('2202', 2202, 1984, 22, 4, 3000, 1000, 1, false, false),\n"
            "('2301', 2301, 1986, 23, 4, 2000, 800, 1, false, false),\n"
            "('2302', 2302, 1992, 23, 4, 3000, 600, 1, false, false)")


def add_space_taz_limits(ps):
    with pr.sd_querier(ps) as q:
        mig.create_taz_groups(q)
        mig.create_tazs_by_taz_group(q)
        mig.create_taz_limit_groups(q)
        mig.create_taz_limit_space_types(q)
        mig.create_space_taz_limits(q)

        with q.transaction() as tr:
            tr.query(
                "insert into {sch}.taz_groups\n"
                "values\n"
                "(1, 'Group One'),\n"
                "(2, 'Group Two'),\n"
                "(3, 'Group Three')")

            tr.query(
                "insert into {sch}.tazs_by_taz_group\n"
                "values\n"
                "(11, 1),\n"
                "(12, 2),\n"
                "(21, 1),\n"
                "(22, 2),\n"
                "(23, 3)")

            tr.query(
                "insert into {sch}.taz_limit_groups\n"
                "values\n"
                "(51, 'Residential'),\n"
                "(53, 'Retail'),\n"
                "(54, 'Office')")

            tr.query(
                "insert into {sch}.taz_limit_space_types\n"
                "values\n"
                "(1, 51),\n"
                "(2, 51),\n"
                "(3, 53),\n"
                "(4, 54)\n")