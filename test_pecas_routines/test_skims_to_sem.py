import csv
import unittest
from os.path import join

import hbautil.scriptutil as su
import pecas_routines.skims_to_sem as ss
from pecas_test import mockscen, mocksettings

ps = mocksettings.PecasSettings().with_machine_settings().with_test_sd().clone_with(
    scenario="Test",
    sql_system="postgres",
    inputpath=".",
    taz_skims_fname="TestLogsums",
    skim_fname="SkimsI",
)


class TestSkimsToSem(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        mockscen.create_database(ps)
        mockscen.create_schema(ps)

    def testAlberta(self):
        folder = join("test", "skims_to_sem", "Alberta")
        su.remove_if_exists(join(folder, "2016", "SkimsI.csv"))
        alberta_ps = ps.with_scenario_directory(folder)
        ss.main(alberta_ps, 2016)
        self.check_skims(join(folder, "ExpectedSkims.csv"), join(folder, "2016", "SkimsI.csv"))

    def testSanDiego(self):
        folder = join("test", "skims_to_sem", "San Diego")
        for fname in ["SkimsI.csv", "SkimsIGoods.csv", "SkimsISOV.csv", "SkimsITransit.csv"]:
            su.remove_if_exists(join(folder, "2016", fname))
        sandiego_ps = ps.with_scenario_directory(folder)
        ss.main(sandiego_ps, 2016)
        self.check_skims(join(folder, "ExpectedSkims.csv"), join(folder, "2016", "SkimsI.csv"))

    def check_skims(self, expected, actual):
        with open(expected) as expf:
            with open(actual) as actf:
                exp_reader = csv.reader(expf)
                act_reader = csv.reader(actf)
                # Skip headers
                header = next(exp_reader)
                next(act_reader)

                for i, (exp_line, act_line) in enumerate(zip(exp_reader, act_reader), start=1):
                    self.assertEqual(
                        int(exp_line[0]), int(act_line[0]), "Mismatch in origin at line {}".format(i))
                    self.assertEqual(
                        int(exp_line[1]), int(act_line[1]), "Mismatch in destination at line {}".format(i))
                    for exp, act, col in zip(exp_line[2:], act_line[2:], header[2:]):
                        if exp.lower() in ["true", "false"]:
                            self.assertEqual(su.tobool(exp), su.tobool(act))
                        else:
                            self.assertAlmostEqual(
                                float(exp), float(act), delta=1e-7, msg="Mismatch at line {}, column {}".format(i, col))
