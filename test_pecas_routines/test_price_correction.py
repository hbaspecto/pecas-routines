import unittest
from os.path import join

import pecas_routines as pr
from pecas_test import mocksettings


class TestPriceCorrection(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.ps = mocksettings.PecasSettings().between_years(2011, 2050)

    def testMaximumSDPricesNothing(self):
        """
        No price caps specified
        """
        ps = self.ps.clone_with(
            scendir=join("test", "maximum_sd_prices", "nothing")
        )
        self.assertEqual({}, pr.read_maximum_sd_prices(ps, 2011))
        self.assertEqual({}, pr.read_maximum_sd_prices(ps, 2016))
        self.assertEqual({}, pr.read_maximum_sd_prices(ps, 2030))

    def testMaximumSDPricesBaseOnly(self):
        """
        Price caps specified once for all years
        """
        ps = self.ps.clone_with(
            scendir=join("test", "maximum_sd_prices", "base_only")
        )
        expected = {"SFD": 150, "MF": 100}
        self.assertEqual(expected, pr.read_maximum_sd_prices(ps, 2011))
        self.assertEqual(expected, pr.read_maximum_sd_prices(ps, 2016))
        self.assertEqual(expected, pr.read_maximum_sd_prices(ps, 2030))

    def testMaximumSDPricesOverrides(self):
        """
        Price caps specified once for all years
        """
        ps = self.ps.clone_with(
            scendir=join("test", "maximum_sd_prices", "overrides")
        )
        self.assertEqual({"SFD": 200, "MF": 100}, pr.read_maximum_sd_prices(ps, 2011))
        self.assertEqual({"SFD": 200, "MF": 150}, pr.read_maximum_sd_prices(ps, 2016))
        self.assertEqual({"SFD": 250, "MF": 250}, pr.read_maximum_sd_prices(ps, 2030))
