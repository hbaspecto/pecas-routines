from typing import Callable

import psycopg2 as pg

from hbautil.sqlutil import Querier


def skip(e, tbl):
    if e.pgcode == "42P07":  # Error code for duplicate table
        print("Table {} already exists; skipping this step".format(tbl))
    else:
        raise e


def run_skipping(q: Querier, step_name: str, function: Callable):
    with q.transaction() as tr:
        try:
            function(tr)
        except pg.ProgrammingError as e:
            skip(e, step_name)
