
import abc
import bisect
import csv
import platform
import shutil
import subprocess
from os.path import join as pathjoin
from typing import Mapping, Any, List

import psycopg2  # library for interacting directly with PostgreSQL for mapit
import sys

import hbautil.csvutil as cu
from hbautil.pecassetup import *


#####


class AARunner(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def run_aa(self, ps, year, skimfile, skimyear, dbyear=None, load=True):
        pass


class SDRunner(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def run_sd(self, ps, year, skimfile=None, skimyear=None):
        pass


class TMRunner(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def run_tm(self, ps, year):
        pass

    @abc.abstractmethod
    def retrieve_skims(self, ps, year):
        pass

    @abc.abstractmethod
    def tm_is_available(self, ps):
        pass

    @abc.abstractmethod
    def tm_precheck(self, ps):
        """
        Performs prechecks specific to the travel model. Returns an error message if the precheck fails,
        None if it succeeds.
        """
        pass


def run_shell_script(ps, fname):
    """
    Runs a bash shell script in a platform-independent way.
    """
    if sys.platform == "win32":
        subprocess.check_call([ps.winShCommand, "-l", fname])
    else:
        subprocess.check_call(fname, shell=True)


def execute_postgresql_query(ps, query, database, port, host, user):
    return subprocess.call(
        [ps.pgpath + "psql", "-c", query, "--dbname=" + database, "--port=" + str(port), "--host=" + host,
         "--username=" + user])


def execute_sqlserver_query(ps, query, database, user, password):
    return subprocess.call(
        [ps.sqlpath + "sqlcmd", "-S", ps.sd_host, "-U", user, "-P", password, "-d", database, "-Q", query])


def check_is_postgres(ps):
    # Error if not using postgres so that we can use psycopg. If we need to
    # use a different database system, functions that call this need to have
    # an alternate version supplied.
    if ps.sql_system != "postgres":
        raise ValueError("This function only works on postgres databases")


def get_native_path():
    string = ""
    if platform.system() == "Windows" or platform.system() == "Microsoft":
        separator = ";"
        string = separator.join(["AllYears/Code/hdf5/win64", ])
    elif platform.system() == "Darwin":
        separator = ":"
        string = separator.join(["AllYears/Code/hdf5/macos", ])
    elif platform.system() == "Linux":
        separator = ":"
        string = separator.join(["AllYears/Code/hdf5/linux64", ])
    return string


def is_aa_year(ps, year):
    return ps.aa_startyear <= year <= ps.stopyear and year in ps.aayears


# Grab the skim year to use given a list of years with new skims
def get_skim_year(year, skimyears):
    i = bisect.bisect_left(skimyears, year) - 1
    if i < 0:
        i = 0
    logging.info("Using skims from year " + str(skimyears[i]) + " for year " + str(year))
    return skimyears[i]


# Function to calculate the base year ratio between the prices that AA is producing and the prices that SD has been
# calibrated to
def calculate_aa_to_sd_price_correction(ps, min_ratio = None, max_ratio = None):
    with open(pathjoin(ps.scendir, str(ps.baseyear), "ExchangeResults.csv"), "r") as fin:
        exchange_results = csv.reader(fin)
        er_header = next(exchange_results)
        try:
            price_column = er_header.index("SellingPrice")
        except ValueError:
            price_column = er_header.index("Price")
        with open(pathjoin(ps.scendir, str(ps.baseyear), "ExchangeResultsTargets.csv"), "r") as fin2:
            er_targets = csv.reader(fin2)
            targets_header = next(er_targets)
            out_path = pathjoin(ps.scendir, "AllYears", "Outputs", "AAtoSDPriceCorrections.csv")
            with open(out_path, "w", newline="") as fout:
                out_writer = csv.writer(fout)
                out_writer.writerow(("Commodity", "LUZ", "PriceCorrection"))
                target_prices = {}
                for row in er_targets:
                    commodity = row[targets_header.index("Commodity")]
                    zone = row[targets_header.index("ZoneNumber")]
                    key = (commodity, zone)
                    price = row[targets_header.index("TargetPrice")]
                    target_prices[key] = price
                for row in exchange_results:
                    commodity = row[er_header.index("Commodity")]
                    zone = row[er_header.index("ZoneNumber")]
                    key = (commodity, zone)
                    if key in target_prices:
                        aa_price = row[price_column]
                        target_price = target_prices[key]
                        price_correction = float(target_price) / float(aa_price)
                        if min_ratio:
                            price_correction = max(price_correction,min_ratio)
                        if max_ratio:
                            price_correction = min(max_ratio,price_correction)
                        out_writer.writerow((commodity, zone, price_correction))


# Function to multiply the AA prices by the previously calculated correction ratio.
def apply_aa_to_sd_price_correction(ps, year):
    maximum_sd_prices = read_maximum_sd_prices(ps, year)
    with open(pathjoin(ps.scendir, str(year), "AAExchangeResults.csv"), "r") as fin:
        exchange_results = csv.reader(fin)
        with open(pathjoin(ps.scendir, "AllYears", "Outputs", "AAtoSDPriceCorrections.csv"), "r") as fin2:
            corrections = csv.reader(fin2)
            with open(pathjoin(ps.scendir, str(year), "ExchangeResults.csv"), "w", newline="") as fout:
                out_writer = csv.writer(fout)
                er_header = next(exchange_results)
                try:
                    price_column = er_header.index("SellingPrice")
                except ValueError:
                    price_column = er_header.index("Price")
                corr_header = next(corrections)
                out_writer.writerow(er_header)
                price_corrections = {}
                for row in corrections:
                    commodity = row[corr_header.index("Commodity")]
                    zone = row[corr_header.index("LUZ")]
                    key = (commodity, zone)
                    price_correction = row[corr_header.index("PriceCorrection")]
                    price_corrections[key] = price_correction
                for row in exchange_results:
                    commodity = row[er_header.index("Commodity")]
                    zone = row[er_header.index("ZoneNumber")]
                    key = (commodity, zone)
                    if key in price_corrections:
                        price = float(row[price_column])
                        newprice = price * float(price_corrections[key])
                        row[price_column] = newprice
                    max_price = maximum_sd_prices.get(commodity, float("inf"))
                    if float(row[price_column]) > max_price:
                        row[price_column] = max_price
                    out_writer.writerow(row)

    logging.info("Replacing ExchangeResults with SD Corrected Version")

    shutil.copyfile(
        pathjoin(ps.scendir, str(year), "ExchangeResults.csv"),
        pathjoin(ps.scendir, str(year), "SDPrices.csv")
    )


def read_maximum_sd_prices(ps, year):
    maximum_prices = _read_maximum_sd_prices_file(
        pathjoin(ps.scendir, "AllYears", "Inputs", "SDPriceCaps.csv")
    )
    for override_year in su.irange(ps.baseyear, year):
        year_maximum_prices = _read_maximum_sd_prices_file(
            pathjoin(ps.scendir, str(override_year), "SDPriceCaps.csv")
        )
        maximum_prices.update(year_maximum_prices)
    return maximum_prices


def _read_maximum_sd_prices_file(path):
    try:
        return cu.read_dict(path, "AASpaceType", "MaximumPrice", value_f=float)
    except IOError:
        return {}


def copy_prices(ps, aa_year):
    shutil.copyfile(
        pathjoin(ps.scendir, str(aa_year), "ExchangeResults.csv"),
        pathjoin(ps.scendir, str(aa_year), "AAExchangeResults.csv")
    )


# Function to adjust the Floorspace by the (previously calculated) correction delta.
def write_floorspace_i(ps, year):
    # can parameterize column names from FloorspaceI in pecas.yml, but this is rarely used
    if hasattr(ps, 'fl_itaz'):
        fl_itaz = ps.flItaz
        fl_icommodity = ps.flIcommodity
        fl_iquantity = ps.flIquantity
    else:
        fl_itaz = "TAZ"
        fl_icommodity = "Commodity"
        fl_iquantity = "Quantity"

    # Read Floorspace O
    with open(pathjoin(ps.scendir, str(year), "FloorspaceO.csv"), "r") as floor_o_in:
        floor_o_in_file = csv.reader(floor_o_in)
        header = next(floor_o_in_file)
        floor_o_dict = {}
        for row in floor_o_in_file:
            key = (row[header.index(fl_itaz)], row[header.index(fl_icommodity)])
            if key in floor_o_dict:
                logging.warning("Line duplicated in FloorspaceO file: %s", key)
                floor_o_dict[key] = floor_o_dict[key] + float(row[header.index(fl_iquantity)])
            else:
                floor_o_dict[key] = float(row[header.index(fl_iquantity)])

    has_c = False
    try:
        # Read floorspace Calc
        with open(pathjoin(ps.scendir, str(year), "FloorspaceCalc.csv"), "r") as floor_c_in:
            floor_c_in_file = csv.reader(floor_c_in)
            header = next(floor_c_in_file)
            floor_c_dict = {}
            for row in floor_c_in_file:
                key = (row[header.index(fl_itaz)], row[header.index(fl_icommodity)])
                if key in floor_c_dict:
                    logging.warning("Line duplicated in FloorspaceCalc file: %s", key)
                    floor_c_dict[key] = floor_c_dict[key] + float(row[header.index(fl_iquantity)])
                else:
                    floor_c_dict[key] = float(row[header.index(fl_iquantity)])

        # Write floorspace Delta
        with open(pathjoin(ps.scendir, str(year), "FloorspaceDelta.csv"), "w", newline="") as floor_d_out:
            floor_d_out_file = csv.writer(floor_d_out)
            header = [fl_itaz, fl_icommodity, fl_iquantity]
            floor_d_out_file.writerow(header)
            key_list = list(floor_c_dict.keys())
            key_list.sort()
            for key in key_list:
                if key in floor_o_dict:
                    delta = floor_c_dict[key] - floor_o_dict[key]
                else:
                    delta = floor_c_dict[key]
                out_row = list(key)
                out_row.append(delta)
                floor_d_out_file.writerow(out_row)

            # Add in ODict values not in CDict; set delta to -ve of ODict value
            key_list = list(floor_o_dict.keys())
            for key in key_list:
                if key in floor_c_dict:
                    pass
                else:
                    delta = -1 * floor_o_dict[key]
                    out_row = list(key)
                    out_row.append(delta)
                    floor_d_out_file.writerow(out_row)

        has_c = True
    except IOError:
        logging.info("NOTICE: FloorspaceCalc not found, using FloorspaceDelta file.")
        shutil.copyfile(
            pathjoin(ps.scendir, str(year - 1), "FloorspaceDelta.csv"),
            pathjoin(ps.scendir, str(year), "FloorspaceDelta.csv")
        )

    # Read floorspace Delta
    with open(pathjoin(ps.scendir, str(year), "FloorspaceDelta.csv"), "r") as floor_d_in:
        floor_d_in_file = csv.reader(floor_d_in)
        header = next(floor_d_in_file)
        floor_d_dict = {}
        for row in floor_d_in_file:
            key = (row[header.index(fl_itaz)], row[header.index(fl_icommodity)])
            if key in floor_d_dict:
                logging.warning("Line duplicated in FloorspaceDelta file: %s", key)
                floor_d_dict[key] = floor_d_dict[key] + float(row[header.index(fl_iquantity)])
            else:
                floor_d_dict[key] = float(row[header.index(fl_iquantity)])

    # Write floorspace I
    if has_c:
        # copy FloorspaceCalc as FloorspaceI
        shutil.copyfile(
            pathjoin(ps.scendir, str(year), "FloorspaceCalc.csv"),
            pathjoin(ps.scendir, str(year), "FloorspaceI.csv")
        )
    else:
        with open(pathjoin(ps.scendir, str(year), "FloorspaceI.csv"), "w", newline="") as floor_i_out:
            floor_i_out_file = csv.writer(floor_i_out)
            header = [fl_itaz, fl_icommodity, fl_iquantity]
            floor_i_out_file.writerow(header)
            key_list = list(floor_d_dict.keys())
            key_list.sort()
            for key in key_list:
                if key in floor_o_dict:
                    net = floor_d_dict[key] + floor_o_dict[key]
                    if net < 0:
                        logging.debug("Negative value for floorspace in %s", key)
                        net = 0
                else:
                    net = floor_d_dict[key]
                    if net < 0:
                        logging.debug("Negative value for floorspace in %s", key)
                        net = 0
                out_row = list(key)
                out_row.append(net)
                floor_i_out_file.writerow(out_row)

            # Add in ODict values not in DDict; set net to ODict value
            key_list = list(floor_o_dict.keys())
            for key in key_list:
                if key in floor_d_dict:
                    pass
                else:
                    net = floor_o_dict[key]
                    if net < 0:
                        logging.debug("Negative value for floorspace in %s", key)
                        net = 0
                    out_row = list(key)
                    out_row.append(net)
                    floor_i_out_file.writerow(out_row)


# Function to calculate and update total imports and exports in ActivityTotalsI in future years (base year has to be
# set before running.

def update_importers_or_exporters(ps, year, activities_to_exclude, activities_to_update, update_using_mor_u):
    query = (
        "update input.activity_totals\n"
        "set total_amount=(case when %(moru)s='U' then -im.amount else im.amount end) from\n"
        "(\n"
        # Inner query is amount made (or used in the case of updating imports) internally last year
            "select commodity,moru, sum(amount) as amount from output.all_makeuse\n"
            "where scenario=%(scen)s and year_run=%(prevyr)s and moru=%(moru)s and activity not like %(actex)s\n"
            "group by commodity, moru\n"
        ") im, output.all_makeuse mu\n"
        "where mu.scenario=%(scen)s and\n"
        "mu.year_run=%(prevyr)s and\n"
        # Next two lines find the exporting activity that used (or importing commodity that made)
        # the commodity we measured in the inner query
        "mu.activity like %(actup)s and\n"
        "mu.commodity=im.commodity\n"
        # next three lines specify the values we are updating, i.e. the appropriate activity/scneario/year
        # combination
        "and mu.activity=input.activity_totals.activity\n"
        "and input.activity_totals.year_run=%(yr)s\n"
        "and input.activity_totals.scenario=%(scen)s;"
    )
    mapit_querier(ps).query(
        query,
        scen=ps.scenario, moru=update_using_mor_u, yr=year, prevyr=year-1,
        actup=activities_to_update, actex=activities_to_exclude
    )


def update_importers(ps, year):
    logging.info("Updating importers")
    update_importers_or_exporters(ps, year, ps.exporter_string, ps.importer_string, 'U')


def update_exporters(ps, year):
    logging.info("Updating exporters")
    update_importers_or_exporters(ps, year, ps.importer_string, ps.exporter_string, 'M')


def load_distances(ps, skim_file_name, skimyear):
    query = "truncate table " + ps.sd_schema + ".distances"
    if ps.sql_system == "postgres":
        retcode = execute_postgresql_query(ps, query, ps.sd_database, ps.sd_port, ps.sd_host, ps.sd_user)
    elif ps.sql_system == "sqlserver":
        retcode = execute_sqlserver_query(ps, query, ps.sd_database, ps.sd_user, ps.sd_password)
    else:
        logging.error("Invalid database system: " + ps.sql_system)
        raise ValueError
    log_results_from_external_program("Deleted old distance data from database",
                                      "Problem deleting old distance data from database", (retcode,))
    with open(pathjoin(ps.scendir, str(skimyear), skim_file_name + ".csv"), "r") as skim_file:
        skim_file_reader = csv.reader(skim_file)
        header = next(skim_file_reader)
        query = "insert into " + ps.sd_schema + ".distances (origin_luz, destination_luz, distance) values "
        first = True
        counter = 0
        for row in skim_file_reader:
            if not first:
                query += ","
            first = False
            origin = row[header.index("Origin")]
            destination = row[header.index("Destination")]
            distance = float(row[header.index(ps.distance_column)])
            if distance == 0:
                distance = 1E99
            query += "(" + str(origin) + "," + str(destination) + "," + str(distance) + ")"
            counter = counter + 1
            if counter >= 500:
                query += ";"
                if ps.sql_system == "postgres":
                    execute_postgresql_query(ps, query, ps.sd_database, ps.sd_port, ps.sd_host, ps.sd_user)
                elif ps.sql_system == "sqlserver":
                    execute_sqlserver_query(ps, query, ps.sd_database, ps.sd_user, ps.sd_password)
                else:
                    logging.error("Invalid database system: " + ps.sql_system)
                    raise ValueError
                query = "insert into " + ps.sd_schema + ".distances  (origin_luz, destination_luz, distance) values "
                first = True
                counter = 0
        if not first:
            query += ";"
            if ps.sql_system == "postgres":
                execute_postgresql_query(ps, query, ps.sd_database, ps.sd_port, ps.sd_host, ps.sd_user)
            elif ps.sql_system == "sqlserver":
                execute_sqlserver_query(ps, query, ps.sd_database, ps.sd_user, ps.sd_password)
            else:
                logging.error("Invalid database system: " + ps.sql_system)
                raise ValueError


def load_exchange_results(ps, year):
    query = "truncate table " + ps.sd_schema + ".exchange_results"
    if ps.sql_system == "postgres":
        retcode = execute_postgresql_query(ps, query, ps.sd_database, ps.sd_port, ps.sd_host, ps.sd_user)
    elif ps.sql_system == "sqlserver":
        retcode = execute_sqlserver_query(ps, query, ps.sd_database, ps.sd_user, ps.sd_password)
    else:
        logging.error("Invalid database system: " + ps.sql_system)
        raise ValueError
    log_results_from_external_program(None, "Problem deleting old exchange results from database", (retcode,))
    # TODO parameterize the filename, so we can load SDPrices instead of ExchangeResults
    with open(pathjoin(ps.scendir, str(year), "ExchangeResults.csv"), "r") as exresults:
        exresults_reader = csv.reader(exresults)
        header = next(exresults_reader)
        # TODO add year column in all_exchange_results, make exchange_results a view for the current year
        query = "insert into " + ps.sd_schema + ".exchange_results (commodity, luz, price, internal_bought) values "
        first = True
        counter = 0
        try:
            price_column = header.index("SellingPrice")
        except ValueError:
            price_column = header.index("Price")
        for row in exresults_reader:
            if not first:
                query += ","
            first = False
            commodity = row[header.index("Commodity")]
            luz = row[header.index("ZoneNumber")]
            price = row[price_column]
            internal_bought = row[header.index("InternalBought")]
            query += "('" + commodity + "'," + luz + "," + price + "," + internal_bought + ")"
            counter = counter + 1
            if counter >= 250:
                query += ";"
                if ps.sql_system == "postgres":
                    execute_postgresql_query(ps, query, ps.sd_database, ps.sd_port, ps.sd_host, ps.sd_user)
                elif ps.sql_system == "sqlserver":
                    execute_sqlserver_query(ps, query, ps.sd_database, ps.sd_user, ps.sd_password)
                else:
                    logging.error("Invalid database system: " + ps.sql_system)
                    raise ValueError
                query = ("insert into " + ps.sd_schema +
                         ".exchange_results  (commodity, luz, price, internal_bought) values ")
                first = True
                counter = 0
        if not first:
            query += ";"
            if ps.sql_system == "postgres":
                execute_postgresql_query(ps, query, ps.sd_database, ps.sd_port, ps.sd_host, ps.sd_user)
            elif ps.sql_system == "sqlserver":
                execute_sqlserver_query(ps, query, ps.sd_database, ps.sd_user, ps.sd_password)
            else:
                logging.error("Invalid database system: " + ps.sql_system)
                raise ValueError


def smooth_prices(ps):
    logging.info("Applying price smoothing")
    query = "update " + ps.sd_schema + ".exchange_results\n\
    set price=new_price from\n\
    ( select commodity, origin_luz, sum(internal_bought) as total_bought, \
    sum(internal_bought * price * power(distance, " + str(ps.gravity_exponent) + \
            ")) / sum(internal_bought * power (distance, " + str(ps.gravity_exponent) + ")) as new_price\n\
    from " + ps.sd_schema + ".exchange_results\n\
    inner join " + ps.sd_schema + ".space_to_commodity\n\
    on exchange_results.commodity=space_to_commodity.aa_commodity\n\
    inner join " + ps.sd_schema + ".distances\n\
    on exchange_results.luz=distances.destination_luz\n\
    group by commodity, origin_luz\n\
    ) new_prices\n\
    where exchange_results.commodity=new_prices.commodity\n\
    and exchange_results.luz=new_prices.origin_luz\n\
    and total_bought > 0"
    if ps.sql_system == "postgres":
        execute_postgresql_query(ps, query, ps.sd_database, ps.sd_port, ps.sd_host, ps.sd_user)
    elif ps.sql_system == "sqlserver":
        execute_sqlserver_query(ps, query, ps.sd_database, ps.sd_user, ps.sd_password)
    else:
        logging.error("Invalid database system: " + ps.sql_system)
        raise ValueError


def write_smoothed_prices(ps, year, pgcmd):
    # TODO use psycopg2 for this
    if ps.sql_system == "sqlserver":
        logging.error("Writing smoothed prices from database not supported yet on SQL Server")
        raise ValueError
    sqlstr = "\\copy (SELECT commodity as \"Commodity\", luz as \"ZoneNumber\"\n\
        , price as \"Price\", internal_bought as \"InternalBought\"\n\
        FROM " + ps.sd_schema + ".exchange_results) to '" +\
        pathjoin(ps.scendir, str(year), "ExchangeResultsSmoothed.csv") + "' csv header"
    retcode = subprocess.check_call(
        [pgcmd, "-c", sqlstr, "--host=" + ps.sd_host, "--port=" + str(ps.sd_port), "--dbname=" + ps.sd_database,
         "--username=" + ps.sd_user])
    log_results_from_external_program(None, "Problem updating ActivityTotalsI in year " + str(year), (retcode,))


def apply_site_spec(ps, year):
    logging.info("Applying site spec")
    query = (
        "update {sch}.parcels p\n"
         "set year_built = s.year_effective, \n"
         "space_type_id = s.space_type_id,\n"
         "space_quantity = s.space_quantity,\n"
         "land_area = s.land_area,\n"
         "is_derelict = false,\n"
         "is_brownfield = false\n"
         "from {sch}.sitespec_parcels s\n"
         "where p.pecas_parcel_num = s.pecas_parcel_num\n"
         "and s.update_parcel = true\n"
         "and s.year_effective = %(yr)s"
    )
    sd_querier(ps).query(query, yr=year)


def update_space_limits(ps, year):
    logging.info("Updating space maximums")
    check_is_postgres(ps)

    with sd_querier(ps).transaction(parcels=ps.parcels_table, yr=year) as tr:
        tr.query(
            "delete from {sch}.space_taz_limits\n"
            "where year_effective >= %(yr)s and manual = false")

        tr.query(
            "insert into {sch}.space_taz_limits\n"
            "select gen.taz_group_id, gen.taz_limit_group_id,\n"
            "gen.min_quantity * "
            "(1 + gen.percent_annual_change_to_min / 100) ^ "
            "(%(yr)s - gen.year_effective),\n"
            "case when gen.max_quantity is null then\n"
            "greatest(sub.total_quantity * "
            "(1 + gen.percent_annual_change_to_max / 100), "
            "sub.total_quantity + gen.min_increment)\n"
            "else\n"
            "gen.max_quantity * "
            "(1 + gen.percent_annual_change_to_max / 100) ^ "
            "(%(yr)s - gen.year_effective)"
            "end\n,"
            "%(yr)s, false\n"
            "from\n"
            "(select distinct on (taz_group_id, taz_limit_group_id) gen.*\n"
            "from {sch}.space_taz_limits_generator gen\n"
            "left join {sch}.space_taz_limits lim\n"
            "on lim.taz_group_id = gen.taz_group_id\n"
            "and lim.taz_limit_group_id = gen.taz_limit_group_id\n"
            "and lim.year_effective <= %(yr)s\n"
            "and lim.year_effective >= gen.year_effective\n"
            "and lim.manual\n"
            "where gen.year_effective <= %(yr)s\n"
            "and lim.taz_group_id is null\n"
            "order by gen.taz_group_id, gen.taz_limit_group_id, gen.year_effective desc\n"
            ") gen\n"
            "left join\n"
            "(select tg.taz_group_id, tlst.taz_limit_group_id, "
            "sum(p.space_quantity) as total_quantity\n"
            "from {sch}.parcels p\n"
            "join {sch}.tazs_by_taz_group tg on tg.taz_number = p.taz\n"
            "join {sch}.taz_limit_space_types tlst "
            "on tlst.space_type_id = p.space_type_id\n"
            "group by tg.taz_group_id, tlst.taz_limit_group_id) sub\n"
            "on sub.taz_group_id = gen.taz_group_id\n"
            "and sub.taz_limit_group_id = gen.taz_limit_group_id\n"
        )


def write_activity_totals(ps, year, dbyear=None):
    if dbyear is None:
        dbyear = year

    query = (
        "select activity as \"Activity\", total_amount as \"TotalAmount\" from input.activity_totals\n"
        "where year_run=%(yr)s and scenario=%(scen)s"
    )
    mapit_querier(ps).dump_to_csv(
        query, pathjoin(ps.scendir, str(year), "ActivityTotalsI.csv"),
        yr=dbyear, scen=ps.scenario
    )


def load_development_events(ps, year):
    logging.info("Loading development events for year:" + str(year))
    if ps.sql_system == "postgres":
        folder_path = os.path.join(ps.scendir, str(year))
        folder_path = folder_path.replace('\\', '/')
        csv_file = os.path.join(folder_path, "developmentEvents.csv")
        conn = connect_to_sd(ps)
        with conn, conn.cursor() as cur:
            cur.execute('TRUNCATE %s.%s' % (ps.sd_schema, "development_events"))  # empty the temporary table
            f = open(csv_file, 'r')  # open the AA output file
            # use the psycopg2 fast copy command to copy the data into the temporary table
            cur.copy_expert('copy "%s"."%s" from STDIN with (format CSV, HEADER, DELIMITER \',\', null \'\')' % (ps.sd_schema, "development_events"), f)
            cur.execute('SELECT count(*) FROM %s.%s;' % (ps.sd_schema, "development_events"))
            counter = cur.fetchone()[0]
            logging.info("Loaded %s development events from file %s" % (counter, str(csv_file)))
    elif ps.sql_system == "sqlserver":
        # TODO Add logging with pyodbc or _mssql
        folder_path = os.path.join(ps.DEVEVENTPATH)
        folder_path = folder_path.replace('/', '\\')
        csv_file = os.path.join(folder_path, "developmentEvents" + str(year) + ".csv")
        # csv_file = os.path.join(folder_path, "developmentEvents.csv")
        query = 'TRUNCATE TABLE %s.%s' % (ps.sd_schema, "development_events")
        execute_sqlserver_query(ps, query, ps.sd_database, ps.sd_user, ps.sd_password)
        f = open(csv_file, 'r')  # open the AA output file
        f.readline()  # skip the first line for header
        query = ("BULK INSERT %s.%s FROM " % (ps.sd_schema, "development_events") +
                 "'" + csv_file + "' WITH (FIELDTERMINATOR = ',',ROWTERMINATOR = '0x0a', FIRSTROW = 2)")
        execute_sqlserver_query(ps, query, ps.sd_database, ps.sd_user, ps.sd_password)
        logging.info("Loaded development events for year:" + str(year))
    else:
        logging.error("Invalid database system: " + ps.sql_system)
        raise ValueError


def load_aa_totals(ps):
    logging.info("Loading Activity Totals for scenario:" + ps.scenario)
    folder_path = os.path.join(ps.scendir, 'AllYears/Inputs')
    folder_path = folder_path.replace('\\', '/')
    conn = connect_to_mapit(ps)
    with conn, conn.cursor() as cur:
        cur.execute('TRUNCATE %s.%s' % ('input', "activity_totals_temp"))  # empty the temporary table
        csv_file = os.path.join(folder_path, "All_ActivityTotalsI.csv")
        f = open(csv_file, 'r')  # open the AA input file
        cur.copy_expert('copy "%s"."%s" from STDIN with (format CSV, HEADER, DELIMITER \',\', null \'\')' % (
            "input", "activity_totals_temp"), f)
        cur.execute("DELETE FROM input.activity_totals WHERE scenario= '%s';" % ps.scenario)
        cur.execute(
            "INSERT INTO input.activity_totals "
            "(select year_run, '%s', activity, total_amount from input.activity_totals_temp);" % (
                ps.scenario))
        cur.execute('SELECT count(*) FROM %s.%s;' % ('input', "activity_totals_temp"))
        counter = cur.fetchone()[0]
        logging.info("Loaded %s activity totals from file %s" % (counter, str(csv_file)))
    if counter == 0:
        logging.fatal("No entries loaded from file " + str(csv_file))
        raise RuntimeError


def load_tm_totals(ps):
    logging.info("Loading Travel Model Totals for scenario:" + ps.scenario)
    folder_path = os.path.join(ps.scendir, 'AllYears/Inputs')
    folder_path = folder_path.replace('\\', '/')
    conn = connect_to_mapit(ps)
    cur = conn.cursor()
    cur.execute('TRUNCATE %s.%s' % ('input', "travel_model_totals_temp"))  # empty the temporary table
    csv_file = os.path.join(folder_path, "TravelModelTotalsI.csv")
    f = open(csv_file, 'r')  # open the AA input file
    cur.copy_expert('copy "%s"."%s" from STDIN with (format CSV, HEADER, DELIMITER \',\', null \'\')' % (
        'input', "travel_model_totals_temp"), f)
    cur.execute("DELETE FROM input.travel_model_totals WHERE scenario= '%s';" % ps.scenario)
    cur.execute(
        "INSERT INTO input.travel_model_totals "
        "(select '%s', year_run, tdm_code, tdm_total_amount from input.travel_model_totals_temp );" % (
            ps.scenario))
    cur.execute('SELECT count(*) FROM %s.%s;' % ('input', "travel_model_totals_temp"))
    counter = cur.fetchone()[0]
    logging.info("Loaded %s travel model totals from file %s" % (counter, str(csv_file)))
    conn.commit()
    conn.close()


def load_am_totals(ps):
    logging.info("Loading Activity Model Totals for scenario:" + ps.scenario)
    folder_path = os.path.join(ps.scendir, 'AllYears/Inputs')
    folder_path = folder_path.replace('\\', '/')
    conn = connect_to_mapit(ps)
    with conn, conn.cursor() as cur:
        cur.execute('TRUNCATE %s.%s' % ('input', "activity_model_totals_temp"))  # empty the temporary table
        csv_file = os.path.join(folder_path, "ABModelTotalsI.csv")
        f = open(csv_file, 'r')  # open the AA input file
        # use the psycopg2 fast copy command to copy the data into the temporary table
        cur.copy_expert('copy "%s"."%s" from STDIN with (format CSV, HEADER, DELIMITER \',\', null \'\')' % (
            'input', "activity_model_totals_temp"), f)
        cur.execute("DELETE FROM input.abm_totals WHERE scenario= '%s';" % ps.scenario)
        cur.execute(
            "INSERT INTO input.abm_totals "
            "(select '%s', year_run, abm_code, abm_total_amount from input.activity_model_totals_temp );" % (
                ps.scenario))
        cur.execute('SELECT count(*) FROM %s.%s;' % ('input', "activity_model_totals_temp"))
        counter = cur.fetchone()[0]
        logging.info("Loaded %s activity model totals from file %s" % (counter, str(csv_file)))


def write_abm_land_use(ps, year, dbyear=None):
    if dbyear is None:
        dbyear = year
    folder_path = os.path.join(ps.scendir, str(year))
    folder_path = folder_path.replace('\\', '/')
    csv_file_name = 'abm_land_use.csv'
    csv_file = os.path.join(folder_path, csv_file_name)

    conn = connect_to_mapit(ps)
    with conn, conn.cursor() as cur:
        query = "delete from output.abm_se_taz10_table where year_run=" + str(
            dbyear) + " and scenario='" + ps.scenario + "';"
        cur.execute(query)
        query = "insert into output.abm_se_taz10_table select * from output.abm_se_taz10 where year_run=" + str(
            dbyear) + " and scenario='" + ps.scenario + "';"
        cur.execute(query)
        query = "copy (select * from output.abm_se_taz10_table where year_run=" + str(
            dbyear) + " and scenario='" + ps.scenario + "') to STDOUT DELIMITER ',' CSV HEADER;"
        with open(csv_file, 'w') as f:
            cur.copy_expert(query, f)


def write_labor_make_use(ps, year, dbyear=None):
    if dbyear is None:
        dbyear = year
    folder_path = os.path.join(ps.scendir, str(year))
    folder_path = folder_path.replace('\\', '/')
    csv_file_name = 'lmau.csv'
    csv_file = os.path.join(folder_path, csv_file_name)

    conn = connect_to_mapit(ps)
    with conn, conn.cursor() as cur:
        query = "copy (select * from output.labor_make_and_use where year_run=" + str(
            dbyear) + " and scenario='" + ps.scenario + "') to STDOUT DELIMITER ',' CSV HEADER;"
        with open(csv_file, 'w') as f:
            cur.copy_expert(query, f)


def replay_development_events_for_year(ps, year):
    load_development_events(ps, year)
    logging.info("Replaying development events for " + str(year))
    if ps.apply_sitespec:
        apply_site_spec(ps, year)
    replay_development_events(ps)
    insert_development_events_into_history(ps, year)


def replay_development_events(ps):
    logging.info("Replaying development events")
    try:
        with sd_querier(ps).transaction(parcels=ps.parcels_table) as tr:
            tr.query(
                '''UPDATE {sch}.parcels parcels SET
                       space_quantity   = development_events.new_space_quantity,
                       space_type_id    = development_events.new_space_type_id,
                       year_built       = development_events.new_year_built,
                       land_area        = development_events.land_area,
                       is_derelict      = development_events.new_is_derelict,
                       is_brownfield    = development_events.new_is_brownfield
                    FROM {sch}.development_events
                    WHERE parcels.pecas_parcel_num = development_events.original_pecas_parcel_num
                       AND (development_events.event_type = 'C' OR
                            development_events.event_type = 'R' OR
                            development_events.event_type = 'D' OR
                           development_events.event_type = 'A' OR
                           development_events.event_type = 'L' OR
                           development_events.event_type = 'US'
                    );''')

            tr.query(
                '''INSERT INTO {sch}.parcels
                        SELECT  parcel_id,
                        new_pecas_parcel_num,
                        new_year_built,
                        taz,
                        new_space_type_id,
                        new_space_quantity,
                        land_area,
                        available_services,
                        new_is_derelict,
                        new_is_brownfield
                    FROM {sch}.development_events
                    WHERE
                         development_events.event_type = 'CS'
                    OR   development_events.event_type = 'AS'
                    OR   development_events.event_type = 'RS'
                    OR   development_events.event_type = 'DS'
                    OR   development_events.event_type = 'LS'
                    ; ''')

            tr.query(
                '''INSERT INTO {sch}.parcel_cost_xref
                        SELECT development_events.new_pecas_parcel_num, parcel_cost_xref.cost_schedule_id,
                        parcel_cost_xref.year_effective
                    FROM {sch}.parcel_cost_xref, {sch}.development_events
                    WHERE parcel_cost_xref.pecas_parcel_num=development_events.original_pecas_parcel_num
                    AND (
                         event_type = 'CS'
                    OR   event_type = 'AS'
                    OR   event_type = 'RS'
                    OR   event_type = 'DS'
                    OR   event_type = 'LS'
                    ); ''')

            tr.query(
                '''INSERT INTO {sch}.parcel_fee_xref
                        SELECT de.new_pecas_parcel_num, xref.fee_schedule_id, xref.year_effective
                    FROM {sch}.parcel_fee_xref xref, {sch}.development_events de
                    WHERE xref.pecas_parcel_num=de.original_pecas_parcel_num
                    AND (
                         event_type = 'CS'
                    OR   event_type = 'AS'
                    OR   event_type = 'RS'
                    OR   event_type = 'DS'
                    OR   event_type = 'LS'
                    ); ''')

            tr.query(
                '''INSERT INTO {sch}.parcel_zoning_xref
                        SELECT de.new_pecas_parcel_num, xref.zoning_rules_code, xref.year_effective
                    FROM {sch}.parcel_zoning_xref xref, {sch}.development_events de
                    WHERE xref.pecas_parcel_num=de.original_pecas_parcel_num
                    AND (
                         event_type = 'CS'
                    OR   event_type = 'AS'
                    OR   event_type = 'RS'
                    OR   event_type = 'DS'
                    OR   event_type = 'LS'
                    ); ''')

            tr.query(
                '''INSERT INTO {sch}.local_effect_distances
                        SELECT de.new_pecas_parcel_num,
                        dist.local_effect_id,
                        dist.local_effect_distance,
                        dist.year_effective
                    FROM {sch}.local_effect_distances dist, {sch}.development_events de
                    WHERE
                    dist.pecas_parcel_num=de.original_pecas_parcel_num
                    AND (
                         event_type = 'CS'
                    OR   event_type = 'AS'
                    OR   event_type = 'RS'
                    OR   event_type = 'DS'
                    OR   event_type = 'LS'
                    );''')
            logging.info("Replayed development events into parcels table")
    except Exception as e:
        logging.error("Problem replaying development events into parcel table: {}".format(e))


def insert_development_events_into_history(ps, year):
    logging.info("Start Inserting Development Events to History" + str(year))
    if ps.sql_system == "postgres":
        with sd_querier(ps).transaction(yr=year) as tr:
            if year == ps.baseyear:
                tr.query("truncate {sch}.development_events_history")
            tr.query(
                "insert into {sch}.development_events_history "
                "(select %(yr)s as year_run, * from {sch}.development_events)"
            )
        logging.info("Completed Inserting Development Events to History: " + str(year))
    elif ps.sql_system == "sqlserver":
        if year == ps.baseyear:
            query = "TRUNCATE table development_events_history; \
            INSERT INTO development_events_history \
            (year_run, \
            event_type, parcel_id, original_pecas_parcel_num, new_pecas_parcel_num, available_services, \
            old_space_type_id, new_space_type_id, old_space_quantity, new_space_quantity, \
            old_year_built, new_year_built, land_area,\
            old_is_derelict, new_is_derelict, \
            old_is_brownfield, new_is_brownfield, \
            zoning_rules_code, taz) (select " + str(year) + " as year_run,* from development_events);"
            retcode = execute_sqlserver_query(ps, query, ps.sd_database, ps.sd_user, ps.sd_password)
            logging.info("Completed Inserting Development Events to History: " + str(year))
        else:
            query = "INSERT INTO development_events_history \
            (year_run, \
            event_type, parcel_id, original_pecas_parcel_num, new_pecas_parcel_num, available_services, \
            old_space_type_id, new_space_type_id, old_space_quantity, new_space_quantity, \
            old_year_built, new_year_built, land_area,\
            old_is_derelict, new_is_derelict, \
            old_is_brownfield, new_is_brownfield, \
            zoning_rules_code, taz) (select " + str(year) + " as year_run,* from development_events);"
            retcode = execute_sqlserver_query(ps, query, ps.sd_database, ps.sd_user, ps.sd_password)
            logging.info("Completed Inserting Development Events to History: " + str(year))
        log_results_from_external_program(
            None, "Problem updating development events history for " + str(year), (retcode,))
    else:
        logging.error("Invalid database system: " + ps.sql_system)
        raise ValueError


def write_floorspace_summary_from_parcel_file(ps, sd_output_year):
    querier = sd_querier(ps)
    try:
        querier.dump_to_csv(
            "select * from {sch}.floorspacei_view", pathjoin(ps.scendir, str(sd_output_year), "FloorspaceI.csv"))
        logging.info("Wrote FloorspaceI from Parcel File for use in year {}".format(sd_output_year))
    except psycopg2.Error:
        logging.fatal("Problem writing FloorspaceI from Parcel File for use in year {}".format(sd_output_year))
        raise


def copy_floorspace_summary(ps, sd_output_year):
    shutil.copyfile(
        pathjoin(ps.scendir, str(sd_output_year), "FloorspaceI.csv"),
        pathjoin(ps.scendir, str(sd_output_year), "FloorspaceSD.csv")
    )
    shutil.copyfile(
        pathjoin(ps.scendir, str(sd_output_year), "FloorspaceI.csv"),
        pathjoin(ps.scendir, str(sd_output_year), "FloorspaceO.csv")
    )


def reset_parcel_database(ps):
    # delete the parcels file and reload from backup
    logging.info("Resetting parcel database in database {} and schema {}".format(
        ps.sd_database, ps.sd_schema))
    try:
        with sd_querier(ps).transaction() as tr:
            logging.info("Deleting local effects for pseudoparcels")
            tr.query(
                "DELETE from {sch}.local_effect_distances "
                "WHERE pecas_parcel_num > (SELECT MAX(pecas_parcel_num) FROM {sch}.parcels_backup);")
            logging.info("Deleting costs for pseudoparcels")
            tr.query(
                "DELETE from {sch}.parcel_cost_xref "
                "WHERE pecas_parcel_num > (SELECT MAX(pecas_parcel_num) FROM {sch}.parcels_backup);")
            logging.info("Deleting fees for pseudoparcels")
            tr.query(
                "DELETE from {sch}.parcel_fee_xref "
                "WHERE pecas_parcel_num > (SELECT MAX(pecas_parcel_num) FROM {sch}.parcels_backup);")
            logging.info("Deleting zoning for pseudoparcels")
            tr.query(
                "DELETE from {sch}.parcel_zoning_xref "
                "WHERE pecas_parcel_num > (SELECT MAX(pecas_parcel_num) FROM {sch}.parcels_backup);")
            logging.info("Deleting pseudoparcels")
            tr.query(
                "DELETE from {sch}.parcels "
                "WHERE pecas_parcel_num > (SELECT MAX(pecas_parcel_num) FROM {sch}.parcels_backup);")
            # tr.query("VACUUM FULL {sch}.parcels;")
            logging.info("Restoring parcels to base year state")
            tr.query("UPDATE {sch}.parcels parcels SET "
                     "year_built = backup.year_built, "
                     "space_type_id = backup.space_type_id, "
                     "space_quantity = backup.space_quantity, "
                     "land_area = backup.land_area, "
                     "available_services_code = backup.available_services_code, "
                     "is_derelict = backup.is_derelict, "
                     "is_brownfield = backup.is_brownfield "
                     "FROM {sch}.parcels_backup backup "
                     "WHERE parcels.pecas_parcel_num = backup.pecas_parcel_num;")

            if ps.adaptive_phasing:
                logging.info("Clearing out current phasing state")
                tr.query("truncate table {sch}.phasing_plan_current")
            logging.info("Finished resetting parcels")
    except Exception as e:
        logging.error("Problem resetting parcels: {}".format(e))

    # restore the zoning rules (in case of phasing changes)
    try:
        with sd_querier(ps).transaction(byr=ps.baseyear) as tr:
            logging.info("Removing zoning after the base year")
            tr.query(
                "delete from {sch}.parcel_zoning_xref where year_effective > %(byr)s"
            )
            logging.info("Restoring base year zoning from the backup")
            tr.query(
                "update {sch}.parcel_zoning_xref xref\n"
                "set zoning_rules_code = backup.zoning_rules_code\n"
                "from {sch}.parcel_zoning_xref_backup backup\n"
                "where xref.pecas_parcel_num = backup.pecas_parcel_num\n"
                "and xref.year_effective = %(byr)s\n"
                "and backup.year_effective = %(byr)s"
            )
            logging.info("Restoring future year zoning from the backup")
            tr.query(
                "insert into {sch}.parcel_zoning_xref\n"
                "select * from {sch}.parcel_zoning_xref_backup\n"
                "where year_effective > %(byr)s"
            )
    except psycopg2.ProgrammingError as e:
        if e.pgcode == "42P01": # Error code for undefined table
            if ps.adaptive_phasing:
                logging.fatal("Cannot restore zoning rules; adaptive phasing requires a zoning rules backup")
                raise
            else:
                logging.warning("No zoning rules backup; above changes have been reverted")
        else:
            raise


def prepare_gale_shapley_outputs(ps, year):
    """
    Prepares the outputs from the Gale-Shapley algorithm in SD to be uploaded to Mapit
    """
    os.environ["CURYEAR"] = str(year)
    run_shell_script(ps, su.in_this_directory(__file__, "prepare_gs_tables.sh"))


def snapshot_parcels(ps, year):
    # Snapshot the parcel database so we can look at intermediate parcel snapshots.
    querier = sd_querier(ps)

    try:
        try:
            with querier.transaction(yr=year) as tr:
                tr.query("drop table if exists {sch}.parcels_{yr};")
                tr.query("create table {sch}.parcels_{yr} "
                         " as select * from {sch}.parcels;")
        except psycopg2.ProgrammingError:
            with querier.transaction(yr=year) as tr:
                # Try truncate-and-insert instead of drop-and-create
                tr.query("truncate table {sch}.parcels_{yr};")
                tr.query("insert into {sch}.parcels_{yr} "
                         " select * from {sch}.parcels;")
        logging.info("Backed up the parcel database for {}".format(year))
    except Exception as e:
        # Don't kill the run if this parcel snapshot doesn't work for some reason
        logging.error("Parcel backup failed: {}".format(e))


class ExternalProgramError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def log_results_from_external_program(okmsg, notokmsg, results_array, allowed_retcodes=(0,)):
    ok = True
    for result in results_array:
        if result not in allowed_retcodes:
            ok = False
    if ok:
        if okmsg is not None:
            logging.info(okmsg)
    else:
        logging.error(notokmsg + ", return codes " + str(results_array))
        raise ExternalProgramError(notokmsg)


def prepare_travel_model_inputs(ps, year, scenario):
    logging.info("Executing query: select input.generate_tm_inputs(" + str(year) + ",'" + scenario + "');")
    retcode = execute_postgresql_query(ps, "select input.generate_tm_inputs(" + str(year) + ",'" + scenario + "');",
                                       ps.mapit_database, ps.mapit_port, ps.mapit_host, ps.mapit_user)
    log_results_from_external_program(None, "Problem preparing table of travel model inputs in database", (retcode,))
