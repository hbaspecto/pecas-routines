import csv
from enum import Enum, auto

import itertools
import logging
import shutil
from os.path import join
import os

import pecas_routines as pr
import hbautil.csvutil as cu
import hbautil.scriptutil as su
from hbautil import settings


# To run on the command line:
# python skims_to_sem.py <skim year>


def main(ps, year):
    squeeze_settings = settings.from_yaml(join(ps.scendir, "skim-squeezing.yml"))
    for squeeze in squeeze_settings.squeezes:
        logging.info("squeezing skims for year "+str(year))
        convert_skims(ps, year, squeeze)

    combined_luz_skims_fname = squeeze_settings.get("combined_luz_skims_fname")
    if combined_luz_skims_fname is not None:
        combine(
            join(ps.scendir, str(year), combined_luz_skims_fname),
            *[
                join(ps.scendir, str(year), squeeze["luz_skims_fname"])
                for squeeze in squeeze_settings.squeezes
            ],
            available_flags=[squeeze.get("available_flag") for squeeze in squeeze_settings.squeezes]
        )


def convert_skims(ps, year, squeeze: dict):
    sql_fname = "skim_conversion_for_sem.sql"

    transport_zones_fname = squeeze.get("transport_zones_fname", "TransportZones.csv")
    transit_access_settings = squeeze.get("transit_access")
    transit_access = TransitAccess(
        ps, year, transit_access_settings
    ) if transit_access_settings else None
    station_numbers_fname = squeeze.get("station_numbers_fname", "StationNumbers.csv")
    taz_skims_fname = squeeze.get("taz_skims_fname",
                                  ps.taz_skims_fname.format(year=year, scenario=ps.scenario) + ".csv")
    luz_skims_fname = squeeze.get("luz_skims_fname", ps.skim_fname.format(year=year) + ".csv")
    square_csv_skims = squeeze.get("square_csv_skims")
    omx_fname = squeeze.get("omx_fname")
    table_suffix = squeeze.get("table_suffix")
    remove_if = squeeze.get("remove_if", "false")
    check_skim = squeeze.get("check_skim")
    externals = squeeze.get("externals", [])
    flow_weighting = squeeze.get("flow_weighting", "MiddayFlows.csv")

    def schemify(name):
        suffix = "" if table_suffix is None else "_" + table_suffix
        return "{}.zz_skim_squeeze{}_{}".format(ps.sd_schema, suffix, name)

    taz_tblname = schemify("tdm_zones")
    world_tblname = schemify("world_zone_skims")
    midday_tblname = schemify("midday_flows")
    tdm_skim_tblname = schemify("tdm_skims")
    taz_skim_tblname = schemify("taz_skims")
    luz_skim_tblname = schemify("luz_skims")

    querier = pr.sd_querier(ps=ps)
    query_maker = SkimQueryMaker(
        tdm_skim_tblname=tdm_skim_tblname,
        world_tblname=world_tblname,
        luz_skim_tblname=luz_skim_tblname,
        squeeze=squeeze,
    )

    transport_zones_path = join(ps.scendir, ps.inputpath, transport_zones_fname)
    station_numbers_path = join(ps.scendir, ps.inputpath, station_numbers_fname)
    taz_skims_path = join(ps.scendir, str(year), taz_skims_fname)

    if omx_fname is not None:
        omx_path = join(ps.scendir, str(year), omx_fname)
        labels_path = transport_zones_path if transit_access is None else station_numbers_path
        extract_from_omx(omx_path, query_maker.input_skim_names, labels_path, taz_skims_path)

    if square_csv_skims is not None:
        skims_path = join(ps.scendir, str(year))
        labels_path = transport_zones_path if transit_access is None else station_numbers_path
        translate_square_skims(
            skims_path=skims_path,
            skim_names=query_maker.input_skim_names,
            transport_zones_path=labels_path,
            taz_skims_path=taz_skims_path
        )

    if transit_access is not None:
        logging.info("Adding transit access")
        station_skims_path = join(ps.scendir, str(year), su.backup_name(taz_skims_fname, "Stations"))
        shutil.copy(taz_skims_path, station_skims_path)
        transit_access.create_skims_with_access(querier, station_skims_path, query_maker.skim_names, taz_skims_path)

    args = dict(
        taztbl=taz_tblname, worldtbl=world_tblname, middaytbl=midday_tblname,
        tdm_skimtbl=tdm_skim_tblname, taz_skimtbl=taz_skim_tblname,
        luz_skimtbl=luz_skim_tblname
    )

    with querier.transaction(**args) as tr:
        skim_fields = query_maker.skim_fields()
        external_skim_fields = query_maker.external_skim_fields()
        logging.info("Loading zone info")

        tr.query("drop table if exists {taztbl}")
        tr.query("create table {taztbl} (tdm_taz integer, luz integer)")
        tr.load_from_csv(taz_tblname, transport_zones_path)

        tr.query("drop table if exists {worldtbl}")
        logging.info("External skim fields are "+str(external_skim_fields))
        tr.query(
            "create table {worldtbl} (\n"
            "   worldmarket integer,\n"
            "   externalstation integer,\n" +
            external_skim_fields +
            ")"
        )
        fieldnames = [("worldmarket" , "integer"),("externalstation","integer")]
        for field in query_maker.external_skim_names:
            fieldnames.append((field , "double precision"))
        tr.load_from_csv(world_tblname, join(ps.scendir, ps.inputpath, "WorldZoneSkims.csv"))

        tr.query("drop table if exists {middaytbl}")
        tr.query("create table {middaytbl} (i integer, j integer, flows double precision, primary key (i, j))")
        # TODO user should specify which flows to use to weight the skims when squeezing them.
        tr.load_from_csv(midday_tblname, join(ps.scendir, ps.inputpath, flow_weighting))

        tr.query("drop table if exists {tdm_skimtbl}")

        tr.query(
            "create table {tdm_skimtbl} (\n"
            "   origin integer,\n"
            "   destination integer,\n" +
            skim_fields +
            ")"
        )

        logging.info("Loading skims for year {}".format(year))

        tr.load_from_csv("{tdm_skimtbl}", taz_skims_path)
        tr.query("delete from {tdm_skimtbl} where " + remove_if)

        logging.info("Adding externals")

        tr.query_external(
            su.in_this_directory(__file__, sql_fname),
            skim_fields=skim_fields,
            best_external_skim_ctes_origin=query_maker.best_external_skim_ctes(OD.ORIGIN),
            best_external_skim_ctes_destination=query_maker.best_external_skim_ctes(OD.DESTINATION),
            best_external_skim_selects=query_maker.best_external_skim_selects(),
            best_external_skim_joins_origin=query_maker.best_external_skim_joins(OD.ORIGIN),
            best_external_skim_joins_destination=query_maker.best_external_skim_joins(OD.DESTINATION),
            end="BREAK",
        )

        logging.info("Analyzing")

        tr.query("analyze {taz_skimtbl}")

        logging.info("Squeezing skims")
        tr.query_external(
            su.in_this_directory(__file__, sql_fname),
            luz_skim_fields=query_maker.luz_skim_fields(),
            luz_skim_sums=query_maker.luz_skim_sums(),
            luz_skim_averages=query_maker.luz_skim_averages(),
            long_distance_logsum_columns=query_maker.long_distance_logsum_columns(),
            start="BREAK",
        )
        check_capping(tr, check_skim, externals=externals)

        logging.info("Dumping LUZ skims")

        tr.dump_to_csv(
            "select * from {luz_skimtbl} order by origin, destination",
            join(ps.scendir, str(year), luz_skims_fname)
        )


def extract_skim_names(name_list):
    def extract_skim_name(name_entry: str):
        name_values = name_entry.split("->") + [""]
        name = name_values[0]
        target_name = name_values[1] or name.split()[0].lower()
        return name.strip(), target_name.strip()

    return su.unzip(map(extract_skim_name, name_list))


def extract_skim_types(name_list):
    def extract_skim_type(name_entry: str):
        name_values = name_entry.split() + [""]
        name = name_values[0]
        skim_type = SkimType.LOGSUM if name_values[1] == "(logsum)" else SkimType.NORMAL
        return name, skim_type

    return su.unzip(map(extract_skim_type, name_list))


class TransitAccess:
    def __init__(self, ps, year, access):
        self.ps = ps
        self.stations_path = join(ps.scendir, ps.inputpath, access["stations_fname"])
        self.access_skim_path = join(ps.scendir, str(year), access["access_skims_fname"])
        self.access_skim_fields = _skim_fields(
            [skim_name.lower() for skim_name in access["access_skim_names"]]
        )
        self.access_skim = access["access_skim"].lower()
        self.access_walk_speed = access.get("access_walk_speed")
        self.externals = access.get("externals", [])

    def create_skims_with_access(self, querier, station_skims_path, skim_names, taz_skims_path):
        ps = self.ps

        def schemify(name):
            return "{}.zz_transit_access_{}".format(ps.sd_schema, name)

        station_tblname = schemify("stations")
        access_skim_tblname = schemify("access_skims")
        station_skim_tblname = schemify("station_skims")
        taz_skim_tblname = schemify("taz_skims")

        args = dict(
            stationtbl=station_tblname,
            access_skimtbl=access_skim_tblname,
            station_skimtbl=station_skim_tblname,
            taz_skimtbl=taz_skim_tblname,
            access_skim=self.access_skim.lower()
        )

        with querier.transaction(**args) as tr:
            tr.query("drop table if exists {stationtbl}")
            tr.query("create table {stationtbl} (station integer primary key, taz integer)")
            tr.load_from_csv(station_tblname, self.stations_path)

            tr.query("drop table if exists {access_skimtbl}")
            tr.query(
                "create table {access_skimtbl} (\n"
                "   origin integer,\n"
                "   destination integer,\n" +
                self.access_skim_fields + ",\n"
                                          "   primary key (origin, destination)\n"
                                          ")"
            )
            tr.load_from_csv(access_skim_tblname, self.access_skim_path)
            tr.query(
                "delete from {access_skimtbl} where origin in %(externals)s or destination in %(externals)s",
                externals=tuple(self.externals)
            )

            tr.query("alter table {access_skimtbl} add column access_skim double precision")
            if self.access_walk_speed is None:
                tr.query("update {access_skimtbl} set access_skim = {access_skim}")
            else:
                tr.query(
                    "update {access_skimtbl} set access_skim = {access_skim} / {walk_speed}",
                    walk_speed=self.access_walk_speed
                )

            tr.query("drop table if exists {station_skimtbl}")
            tr.query(
                "create table {station_skimtbl} (\n"
                "   origin_station integer,\n"
                "   destination_station integer,\n" +
                _skim_fields(skim_names) +
                ",\n"
                "   primary key (origin_station, destination_station)\n"
                ")"
            )
            tr.load_from_csv(station_skim_tblname, station_skims_path)

            tr.query_external(su.in_this_directory(__file__, "transit_access.sql"))
            tr.dump_to_csv("select * from {taz_skimtbl}", taz_skims_path)


def translate_square_skims(
        skims_path: str,
        skim_names,
        taz_skims_path: str,
        transport_zones_path: str
    ):
    """

    :param skims_path: The path to the skims to be loaded
    :param skim_names: The filenames (without the .csv extension) of the square csv files with origins in rows and destinations in columns
    :param taz_skims_path: The i,j,v,v,v,  file to be written out
    :return: None (writes it's results to a file that is then re-read)
    """

    logging.info("Reading csv skims")

    zones = read_zones(transport_zones_path)

    matrices = []
    for name in skim_names:
        # TODO for now this method is assuming that the rows and columns match up in the correct order
        m = read_square_csv_matrix(skims_path, name)
        matrices.append(m)

    # Create header
    result = [["i", "j"] + skim_names]

    for i_idx, i in enumerate(zones):
        for j_idx, j in enumerate(zones):
            result.append([i,j] + [matrix[i_idx][j_idx+1] for matrix in matrices])

    write_skims(taz_skims_path, result)



def read_square_csv_matrix(
        skims_dir: str,
        name: str
    ):
    """

    :param skims_dir: the directory where the skim is
    :param name: the name of the skim, also the name of the file before the .csv extension
    :return: a 2D numpy array containing the skims
    """
    import numpy as np
    file_path = os.path.join(skims_dir, name+".csv")
    logging.info("Reading square csv file "+file_path)
    r = np.genfromtxt(
        file_path,
        delimiter=',',
        names=True,
        case_sensitive=True,
        invalid_raise=True)
    # TODO write a class that can access it with subscript notation that looks up the correct row and column
    return r


def extract_from_omx(omx_fname, skim_names, transport_zones_path, taz_skims_path):
    import openmatrix as omx
    import numpy as np
    logging.info("Extracting skims from {}".format(omx_fname))
    # noinspection PyTypeChecker
    f: omx.File = omx.open_file(omx_fname)
    try:
        zones = read_zones(transport_zones_path)

        matrices = [np.array(f[name]) for name in skim_names]

        result = [["i", "j"] + skim_names]
        for i_idx, i in enumerate(zones):
            for j_idx, j in enumerate(zones):
                result.append([i, j] + [matrix[i_idx, j_idx] for matrix in matrices])

        write_skims(taz_skims_path, result)

    finally:
        f.close()


def read_zones(path):
    with open(path, "r") as f:
        reader = csv.reader(f)
        header = next(reader)
        taz_col = header.index("taz")
        return [row[taz_col] for row in reader]


def write_skims(path, skim_data):
    with open(path, "w", newline="") as f:
        writer = csv.writer(f)
        for row in skim_data:
            writer.writerow(row)


class SkimQueryMaker:
    def __init__(
            self,
            tdm_skim_tblname,
            world_tblname,
            luz_skim_tblname,
            squeeze,
    ):
        self.tdm_skim_tblname = tdm_skim_tblname
        self.world_tblname = world_tblname
        self.luz_skim_tblname = luz_skim_tblname

        self.skim_names = squeeze["skim_names"]
        taz_skim_renames = su.flip_dict(squeeze.get("taz_skim_renames", {}))
        self.input_skim_names = [
            taz_skim_renames.get(skim_name, skim_name) for skim_name in self.skim_names
        ]
        logsum_skims = squeeze.get("logsum_skims", [])
        self.skim_names_and_types = [
            (skim_name, SkimType.LOGSUM if skim_name in logsum_skims else SkimType.NORMAL)
            for skim_name in self.skim_names
        ]
        self.skim_names_to_types = dict(self.skim_names_and_types)

        self.external_skim_names = squeeze["external_skim_names"]
        skims_to_external_skims = squeeze.get("skims_to_external_skims", {})
        self.skims_to_external_skims = {
            skim_name: skims_to_external_skims.get(skim_name, skim_name)
            for skim_name in self.skim_names
            if skim_name in skims_to_external_skims or skim_name in self.external_skim_names
        }
        self.best_external_skim_names = squeeze["best_external_skim_names"]
        self.best_external_skims_to_skims = {
            external_skim_name: skim_name
            for skim_name, external_skim_name in self.skims_to_external_skims.items()
            if external_skim_name in self.best_external_skim_names
        }

        self.luz_skim_names = squeeze.get("luz_skim_names", self.skim_names)
        luz_skim_renames = su.flip_dict(squeeze.get("luz_skim_renames", {}))
        luz_skim_aliases = squeeze.get("luz_skim_aliases", {})
        self.luz_skims = [
            (luz_skim_name, skim_name, self.skim_names_to_types[skim_name])
            for luz_skim_name in self.luz_skim_names
            for luz_skim_alias in [luz_skim_aliases.get(luz_skim_name, luz_skim_name)]
            for skim_name in [luz_skim_renames.get(luz_skim_alias, luz_skim_alias)]
        ]

        self.long_distance_logsums = squeeze.get("long_distance_logsums")

    def skim_fields(self):
        return _skim_fields(self.skim_names)

    def external_skim_fields(self):
        return _skim_fields(self.external_skim_names)

    def best_external_skim_ctes(self, origin_or_destination: 'OD'):
        this_end, that_end = self._ends(origin_or_destination)
        return ",\n".join(
            f"best{external_skim_name} as (\n"
            f"  select tzs.{this_end}, wm.worldmarket, wm.externalstation,\n"
            f"  tzs.{skim_name} + wm.{external_skim_name} as {external_skim_name}\n"
            f"  from {self.tdm_skim_tblname} tzs\n"
            f"  join {self.world_tblname} wm\n"
            f"  on tzs.{that_end} = wm.externalstation\n"
            f"  order by tzs.{this_end}, wm.worldmarket,\n"
            f"  tzs.{skim_name} + wm.{external_skim_name}{' desc' if skim_type == SkimType.LOGSUM else ''}\n"
            f")"
            for external_skim_name in self.best_external_skim_names
            for skim_name in [
                self.best_external_skims_to_skims[
                    external_skim_name
                ]
            ]
            for skim_type in [
                self.skim_names_to_types[skim_name]
            ]
        )

    def best_external_skim_selects(self):
        return ",\n".join(
            f"best{skim_name}.{skim_name}"
            if skim_name in self.best_external_skim_names
            else f"tz3o.{skim_name}" + (
                "" if external_skim_name is None else f" + wm.{external_skim_name}"
            )
            for skim_name in self.skim_names
            for external_skim_name in [
                self.skims_to_external_skims.get(skim_name)
            ]
        )

    def best_external_skim_joins(self, origin_or_destination: 'OD'):
        this_end, that_end = self._ends(origin_or_destination)
        return "\n".join(
            f"join best{skim_name} "
            f"on tz3o.{this_end} = best{skim_name}.{this_end} "
            f"and tz3o.{that_end} = best{skim_name}.externalstation "
            f"and best{skim_name}.worldmarket = wm.worldmarket"
            for skim_name in self.best_external_skim_names
        )

    @staticmethod
    def _ends(origin_or_destination: 'OD'):
        if origin_or_destination == OD.ORIGIN:
            return "origin", "destination"
        else:
            return "destination", "origin"

    def luz_skim_fields(self):
        return _skim_fields(self.luz_skim_names)

    def luz_skim_sums(self):
        return ",\n".join(
            [
                f"{'max' if skim_type == SkimType.LOGSUM else 'min'}(ts.{skim_name}) as best_{skim_name},\n"
                f"sum({sum_expression}) as total_{skim_name}"
                for skim_name, skim_type in self.skim_names_and_types
                for weighted_term in [
                    f"ts.{skim_name} * f.flows"
                ]
                for sum_expression in [
                    f"case when ts.{skim_name} = '-Infinity' then 0 else {weighted_term} end"
                    if skim_type == SkimType.LOGSUM
                    else weighted_term
                ]
            ] + [
                f"sum(case when ts.{skim_name} = '-Infinity' then 0 else f.flows end) as total_flows_{skim_name}"
                for skim_name, skim_type in self.skim_names_and_types
                if skim_type == SkimType.LOGSUM
            ] + ["sum(f.flows) as total_flows"]
        )

    def luz_skim_averages(self):
        return ",\n".join(
            f"case when total_flows = 0 then best_{skim_name} "
            f"else total_{skim_name} / "
            f"total_flows{'_' + skim_name if skim_type == SkimType.LOGSUM else ''} "
            f"end"
            for _, skim_name, skim_type in self.luz_skims
        )

    def long_distance_logsum_columns(self):
        if self.long_distance_logsums is None:
            return ""
        return (
            f"alter table {self.luz_skim_tblname}\n" +
            ",\n".join(
                f"add column {logsum_skim}_{int(distance_reference)} double precision"
                for logsum_skim, distances in self.long_distance_logsums.items()
                for distance in distances["distances"]
                for distance_reference in [distance["distance_reference"]]
            ) +
            ";\n\n"
            f"update {self.luz_skim_tblname} set\n" +
            ",\n".join(
                f"{logsum_skim}_{int(distance_reference)} = "
                f"{logsum_skim} / (1 + ({distance_skim} - {float(distance_cutoff)}) / {float(distance_reference)})"
                for logsum_skim, distances in self.long_distance_logsums.items()
                for distance_skim in [distances["distance_skim"]]
                for distance in distances["distances"]
                for distance_cutoff in [distance["distance_cutoff"]]
                for distance_reference in [distance["distance_reference"]]
            ) +
            ";"
        )


class OD(Enum):
    ORIGIN = auto()
    DESTINATION = auto()


class SkimType(Enum):
    NORMAL = auto()
    LOGSUM = auto()


def _skim_fields(skim_names):
    return ",\n".join(
        "   \"{}\" double precision".format(skim_name)
        for skim_name in skim_names
    )


def check_capping(tr, skim_name, externals):
    result = tr.query(
        "select origin_luz,\n"
        "count(distinct destination_luz) from {taz_skimtbl}\n"
        "where {skim_name} > 2000\n"
        "and origin_luz not in %(externals)s and destination_luz not in %(externals)s\n"
        "group by origin_luz\n"
        "order by count(distinct destination_luz) desc;",
        skim_name=skim_name,
        externals=tuple(externals)
    )

    if result:
        logging.error("There are skims over 2000 minutes!")
        for row in result:
            logging.error("Found {} bad skims from origin {}".format(row[1], row[0]))
        raise AssertionError()


def combine(dest_path, *source_paths, available_flags=None):
    if available_flags is None:
        available_flags = itertools.repeat(None)
    source_skims = list(map(read_skims, source_paths))
    keys = sorted(set(source_skims[0].keys()).union(*source_skims[1:]))
    result = {key: [] for key in keys}
    header = ["origin", "destination"]
    for skims, available_flag in zip(source_skims, available_flags):
        skim_names = list(next(iter(skims.values())).keys())
        if available_flag is not None:
            header.append(available_flag)
        header.extend(skim_names)
        for key, row in result.items():
            if available_flag is None:
                if key in skims:
                    row.extend(skims[key].values())
                else:
                    raise ValueError("No skims for origin {}, destination {}".format(*key))
            else:
                if key in skims:
                    row.append("true")
                    row.extend(skims[key].values())
                else:
                    row.append("false")
                    row.extend([0 for name in skim_names])
    result_rows = [header] + [list(key) + row for key, row in result.items()]
    write_skims(dest_path, result_rows)


def read_skims(path):
    def row_f(key):
        o, d = key
        return int(o), int(d)

    return cu.read_table(path, rowh_size=2, row_f=row_f, value_f=float)


def run_default():

    import sys

    main_ps = pr.load_pecas_settings()
    skim_year = sys.argv[1]
    skim_year = int(skim_year)
    pr.set_up_logging(main_ps)
    main(main_ps, skim_year)


if __name__ == "__main__":
    run_default()
