import logging

import pecas_routines as pr


# Adaptive Phasing Algorithm

def clear_manual_zoning_in_phasing_plans(ps):
    with pr.sd_querier(ps).transaction() as tr:
        logging.info("Removing manual zoning in phasing plans")
        tr.query(
            "delete from {sch}.parcel_zoning_xref\n"
            "where pecas_parcel_num in\n"
            "(select pecas_parcel_num from {sch}.phasing_plan_xref)"
        )
        logging.info("Adding base year unreleased zoning for phasing plans")
        tr.query(
            "insert into {sch}.parcel_zoning_xref\n"
            "select pecas_parcel_num, %(unreleased)s, %(byr)s\n"
            "from {sch}.phasing_plan_xref",
            unreleased=ps.adaptive_phasing_unreleased,
            byr=ps.baseyear
        )


def update_parcel_zoning_xref_according_to_adaptive_phasing(ps, current_year):
    plan_release_year_threshold_dict = {}
    with pr.sd_querier(ps).transaction() as tr:
        plan_and_smallest_phase = tr.query(
            "select x.plan_id, initial_release_year, min(phase_number) from\n"
            "{sch}.phasing_plan_xref x INNER JOIN {sch}.phasing_plans ph\n"
            "ON ph.plan_id = x.plan_id\n"
            "group by (x.plan_id, initial_release_year, release_threshold)"
        )
        for i in plan_and_smallest_phase:
            # Store [initial release year, smallest phase] pair for each plan
            plan_release_year_threshold_dict[i[0]] = [i[1], i[2]]
        for plan_id, [initial_release_year, min_phase_number] in plan_release_year_threshold_dict.items():
            if initial_release_year == current_year or (
                    current_year == ps.baseyear and initial_release_year <= current_year):
                # Will start next year, so update the table and put initial year into year_effective
                insert_into_parcel_zoning_xref_table(ps, current_year, min_phase_number, plan_id)
                tr.query(
                    "update {sch}.phasing_plan_current\n"
                    "set cur_phase = %(curph)s\n"
                    "where plan_id = %(plan)s",
                    plan=plan_id, curph=min_phase_number
                )

            elif initial_release_year < current_year:
                # Already started or just started
                recent_developed_phase = tr.query(
                    "select cur_phase from {sch}.phasing_plan_current\n"
                    "where plan_id = %(plan)s",
                    plan=plan_id
                )[0][0]
                if check_threshold(ps, plan_id, recent_developed_phase):
                    # Threshold exceeded
                    next_phase = find_next_phase_of_the_plan(ps, plan_id, recent_developed_phase)
                    tr.query(
                        "update {sch}.phasing_plan_current\n"
                        "set cur_phase = %(curph)s\n"
                        "where plan_id = %(plan)s",
                        plan=plan_id, curph=next_phase
                    )
                    if next_phase:
                        insert_into_parcel_zoning_xref_table(ps, current_year, next_phase, plan_id)
                    else:
                        # There is no next phase, go on to the next plan
                        continue
            else:
                # Haven't started yet, go on to the next plan
                continue


def insert_into_parcel_zoning_xref_table(ps, current_year, next_phase, plan_id):
    # next year
    with pr.sd_querier(ps).transaction(yr=current_year, phase=next_phase, plan=plan_id) as tr:
        query = (
            "insert into {sch}.parcel_zoning_xref\n"
            "select pzx.pecas_parcel_num, ppx.zoning_rules_code, %(yr)s\n"
            "from {sch}.parcel_zoning_xref as pzx, {sch}.phasing_plan_xref as ppx\n"
            "where pzx.pecas_parcel_num = ppx.pecas_parcel_num\n"
            "and pzx.pecas_parcel_num in\n"
            "(select pecas_parcel_num from {sch}.phasing_plan_xref\n"
            "where phase_number = %(phase)s and plan_id = %(plan)s)\n"
            "on conflict (pecas_parcel_num, year_effective)\n"
            "do update set zoning_rules_code=excluded.zoning_rules_code"
        )
        tr.query(query)


def find_next_phase_of_the_plan(ps, plan_id, current_phase):
    with pr.sd_querier(ps).transaction(phase=current_phase, plan=plan_id) as tr:
        next_phase = tr.query(
            "select min(phase_number)\n"
            "from {sch}.parcel_zoning_xref as pzx, {sch}.phasing_plan_xref as ppx\n"
            "where pzx.pecas_parcel_num = ppx.pecas_parcel_num\n"
            " and ppx.plan_id = %(plan)s and phase_number > %(phase)s")

        if len(next_phase) > 0:
            # The result should have only one row containing only one value, so just get it
            next_phase = next_phase[0][0]
            logging.info("Plan {} is starting a new phase: {}".format(plan_id, next_phase))
            return next_phase
        else:
            return None


def check_threshold(ps, plan_id, phase_id):
    parcels_table = ps.parcels_table
    if phase_id is None:
        return False

    undeveloped_types = ",".join(["{}".format(x) for x in ps.undeveloped_phasing_types])

    with pr.sd_querier(ps).transaction(parcels=parcels_table,
            phase=phase_id,
            plan=plan_id,
            undeveloped_types=undeveloped_types) as tr:
        #logging.info("Checking threshold for plan {}".format(plan_id))
        # get summation of land_area for the most recently developed phase
        total_developable_area = tr.query(
            "select sum(land_area)\n"
            "from {sch}.{parcels}\n"
            "where pecas_parcel_num in\n"
            "(select pzx.pecas_parcel_num\n"
            "from {sch}.parcel_zoning_xref as pzx, {sch}.{parcels} as ppo, {sch}.phasing_plan_xref as ppx\n"
            "where ppx.plan_id = %(plan)s and ppx.phase_number = %(phase)s\n"
            "and pzx.pecas_parcel_num = ppo.pecas_parcel_num\n"
            "and pzx.pecas_parcel_num = ppx.pecas_parcel_num\n"
            "and ppx.zoning_rules_code in\n"
            "(select zoning_rules_code from {sch}.zoning_permissions)) ")[0][0]


        current_developed_area = tr.query(
'''       select sum(p.land_area)
            from {sch}.{parcels} as p
                        join {sch}.phasing_plan_xref as ppx using (pecas_parcel_num)
            where p.space_type_id not in ({undeveloped_types})
                        and ppx.plan_id =  %(plan)s and ppx.phase_number = %(phase)s
						and p.space_quantity > 0
                        and  ppx.zoning_rules_code in
            (select zoning_rules_code from {sch}.zoning_permissions)''')[0][0]


        #logging.info('Total Area: {}'.format(total_developable_area))
        #logging.info('Current Area: {}'.format(current_developed_area))
        if total_developable_area is None:
            logging.warning('No developable area!!!')
            return True
        if current_developed_area is None:
            current_developed_area = 0
        current_developed_fraction = current_developed_area / total_developable_area
        threshold_rows = tr.query(
            "select release_threshold from {sch}.phasing_plans where plan_id = %(plan)s")
        if len(threshold_rows) == 0:
            logging.error("Plan {} is not defined!".format(plan_id))
            raise ValueError
        threshold = threshold_rows[0][0]

        #logging.info('Current Threshold: {}'.format(threshold))

        if current_developed_fraction > threshold:
            logging.info('Threshold Exceeded for plan {}'.format(plan_id))
            return True
        else:
            #logging.info('Still Below Threshold')
            return False


def set_plan_phase_dict(ps):
    with pr.sd_querier(ps).transaction(parcels=ps.parcels_table) as tr:
        query = (
            "insert into {sch}.phasing_plan_current\n"
            "select distinct plan_id, 0 from {sch}.phasing_plans"
        )
        tr.query(query)
