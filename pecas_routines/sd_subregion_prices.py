import csv
import shutil
from os.path import join

from hbautil import scriptutil as su


def main(ps, luzs):
    for year in su.irange(ps.baseyear, ps.endyear):
        in_path = join(ps.scendir, str(year), "ExchangeResults.csv")
        backup_path = join(ps.scendir, str(year), "ExchangeResultsFull.csv")
        shutil.copy(in_path, backup_path)
        with open(backup_path) as inf:
            reader = csv.reader(inf)
            header = next(reader)
            luz_col = header.index("ZoneNumber")
            with open(in_path, "w", newline="") as outf:
                writer = csv.writer(outf)
                writer.writerow(header)
                for line in reader:
                    if int(line[luz_col]) in luzs:
                        writer.writerow(line)
