import random

import psycopg2 as pg
import sys

import pecas_routines as pr
from pecas_routines.migrate import skip


def main(ps, adaptive_phasing=False):
    querier = pr.sd_querier(ps)
    with querier:
        create_random_seeds(querier)
        create_density_step_points(querier)
        if adaptive_phasing:
            create_adaptive_phasing(querier)


def create_random_seeds(q):
    with q.transaction() as tr:
        try:
            stmt = (
                "create table {sch}.random_seeds\n"
                "(\n"
                "   random_seed_index integer primary key,\n"
                "   random_seed bigint\n"
                ")"
            )

            tr.query(stmt)

            rnd = random.Random(1234)
            rnd_numbers = [
                (i, rnd.randint(-(2 ** 63), 2 ** 63 - 1))
                for i in range(2 ** 16)]

            stmt = (
                "insert into {sch}.random_seeds\n"
                "values (%s, %s)"
            )

            tr.query_many(stmt, rnd_numbers)

            print("Created table random_seeds")
        except pg.ProgrammingError as e:
            skip(e, "random_seeds")


def create_density_step_points(q):
    with q.transaction() as tr:
        try:
            stmt = (
                "create table {sch}.density_step_points\n"
                "(space_type_id integer not null,\n"
                "step_point_number integer not null,\n"
                "step_point_intensity double precision,\n"
                "slope_adjustment double precision,\n"
                "step_point_adjustment double precision,\n"
                "constraint density_step_points_pkey primary key\n"
                "(space_type_id, step_point_number),\n"
                "constraint density_step_points_fkey foreign key (space_type_id)\n"
                "references {sch}.space_types_i (space_type_id)\n"
                "match simple on update cascade on delete cascade\n"
                ")"
            )

            tr.query(stmt)

            stmt = (
                "insert into {sch}.density_step_points\n"
                "select space_type_id, 1,\n"
                "min_intensity, below_step_point_adjustment, 0\n"
                "from {sch}.space_types_i"
            )

            tr.query(stmt)

            stmt = (
                "insert into {sch}.density_step_points\n"
                "select space_type_id, 2,\n"
                "step_point, above_step_point_adjustment, step_point_adjustment\n"
                "from {sch}.space_types_i"
            )

            tr.query(stmt)

            print("Created table density_step_points")

            stmt = (
                "alter table {sch}.space_types_i\n"
                "drop column below_step_point_adjustment,\n"
                "drop column above_step_point_adjustment,\n"
                "drop column step_point,\n"
                "drop column step_point_adjustment"
            )

            tr.query(stmt)

            print("Dropped obsolete columns from table space_types_i")
        except pg.ProgrammingError as e:
            skip(e, "density_step_points")


def create_adaptive_phasing(q):
    with q.transaction() as tr:
        try:
            stmt = (
                "create table {sch}.phasing_plans\n"
                "(\n"
                "plan_id integer primary key,\n"
                "initial_release_year integer,\n"
                "release_threshold double precision check (release_threshold between 0 and 0.7)\n"
                ")"
            )

            tr.query(stmt)
            print("Created table phasing_plans")

            stmt = (
                "create table {sch}.phasing_plan_xref\n"
                "(\n"
                "pecas_parcel_num bigint primary key\n"
                "references {sch}.parcels (pecas_parcel_num) on delete cascade,\n"
                "plan_id integer references {sch}.phasing_plans (plan_id),\n"
                "phase_number integer,\n"
                "zoning_rules_code integer references {sch}.zoning_rules_i(zoning_rules_code)\n"
                ")"
            )

            tr.query(stmt)
            print("Created table phasing_plan_xref")

            stmt = (
                "create table {sch}.phasing_plan_current\n"
                "(\n"
                "plan_id integer primary key references {sch}.phasing_plans (plan_id),\n"
                "cur_phase integer\n"
                ")"
            )

            tr.query(stmt)
            print("Created table phasing_plan_current")
        except pg.ProgrammingError as e:
            skip(e, "phasing_plans")


def run_default():
    args = set(sys.argv[1:])
    phasing = "phasing" in args
    main_ps = pr.load_pecas_settings()
    main(main_ps, adaptive_phasing=phasing)


if __name__ == "__main__":
    run_default()
