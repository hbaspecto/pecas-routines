-- This SQL is called by employment.py, which substitutes the {} values.  It 
-- assigns employment to TAZs based on the changes in labour use amounts from AA.


-- Map base and current labour to NAICS and put group categories.

drop table if exists {sch}.zz_employment_use;

create table {sch}.zz_employment_use as
select taz, activity, put,
putg.labour_group, actnaics.naics_minus as naics,
coalesce(-base_emp.amount, 0) as base_amount, coalesce(-fut_emp.amount, 0) as fut_amount
from
(
    -- Ensure we get have a value for all possible combinations so we don't have to coalesce everywhere else.
    select u1.taz, u2.activity, u3.put
    from (select taz from {taztbl}) u1
    cross join (select distinct activity from {baseuse}) u2
    cross join (select distinct put from {baseuse}) u3
) all_combinations
left join {baseuse} base_emp
using (activity, taz, put)
left join {curuse} fut_emp
using (activity, taz, put)
join {puttbl} putg
using (put)
join {acttbl} actnaics
using (activity);

alter table {sch}.zz_employment_use add primary key (taz, activity, put);

-- WAH-start

drop table if exists {sch}.zz_employment_use_work_at_work;

alter table {sch}.zz_employment_use
rename to zz_employment_use_work_at_work;

-- Find the province-wide splits from labour type to NAICS code.

drop table if exists {sch}.zz_employment_put_to_naics;

create table {sch}.zz_employment_put_to_naics as
select put, naics, base_amount, fut_amount,
base_amount / sum(base_amount) over (partition by put) as base_frac,
fut_amount / sum(fut_amount) over (partition by put) as fut_frac
from
(
    select put, naics,
    sum(base_amount) as base_amount, sum(fut_amount) as fut_amount
    from {sch}.zz_employment_use_work_at_work
    group by put, naics
) sub;

alter table {sch}.zz_employment_put_to_naics add primary key (put, naics);

-- Find the employment distribution that would happen if everyone worked from home, using the detailed make

drop table if exists {sch}.zz_employment_use_work_at_home;

create table {sch}.zz_employment_use_work_at_home as
select taz, putnaics.naics, put, putg.labour_group,
coalesce(base_emp.amount, 0) * putnaics.base_frac as base_amount,
coalesce(fut_emp.amount, 0) * putnaics.fut_frac as fut_amount
from
(
    -- Ensure we get have a value for all possible combinations so we don't have to coalesce everywhere else.
    select u1.taz, u2.put
    from (select taz from {taztbl}) u1
    cross join (select distinct put from {basemake}) u2
) all_combinations
left join
(
    select taz, put, sum(amount) as amount
    from {basemake}
    group by taz, put
) base_emp
using (taz, put)
left join
(
    select taz, put, sum(amount) as amount
    from {curmake}
    group by taz, put
) fut_emp
using (taz, put)
join {puttbl} putg
using (put)
join {sch}.zz_employment_put_to_naics putnaics
using (put);

alter table {sch}.zz_employment_use_work_at_home add primary key (taz, naics, put);

-- Blend the regular workers with the work-at-home workers, according to the work-at-home rates by put

create table {sch}.zz_employment_use as
select taz, naics, put, waw.labour_group,
wahfrac.wahfrac * wah.base_amount + (1 - wahfrac.wahfrac) * waw.base_amount as base_amount,
wahfrac.wahfrac * wah.fut_amount + (1 - wahfrac.wahfrac) * waw.fut_amount as fut_amount
from
(
    select taz, naics, put, labour_group, sum(base_amount) as base_amount, sum(fut_amount) as fut_amount
    from {sch}.zz_employment_use_work_at_work
    group by taz, naics, put, labour_group
) waw
join {sch}.zz_employment_use_work_at_home wah
using (taz, naics, put)
join
(
    select put, work_at_home_fraction as wahfrac from {wahtbl}
) wahfrac
using (put);


-- WAH-end

-- Find the differences between the current and base amounts

alter table {sch}.zz_employment_use
add column diff double precision;

update {sch}.zz_employment_use
set diff = fut_amount - base_amount;

-- Aggregate to the whole province by NAICS and put group and divide by the average wages.

drop table if exists {sch}.zz_employment_jobs_by_naics_putgroup;

create table {sch}.zz_employment_jobs_by_naics_putgroup as
select labour_group, naics, use.base_amount, use.fut_amount, use.diff, use.diff / wage.average_wage as new_jobs
from
(
    select labour_group, naics,
    sum(base_amount) as base_amount, sum(fut_amount) as fut_amount, sum(diff) as diff
    from {sch}.zz_employment_use
    group by labour_group, naics
) use
join {wagetbl} wage
using (labour_group, naics);

alter table {sch}.zz_employment_jobs_by_naics_putgroup add primary key (labour_group, naics);

-- Find TAZ/put group combinations that have decreased in labour amounts

drop table if exists {sch}.zz_employment_decrements;

create table {sch}.zz_employment_decrements as
select taz, labour_group, base_amount, diff,
case when -diff > base_amount then 0 else 1 + diff / base_amount end as scale_factor
from
(
    select taz, labour_group, sum(base_amount) as base_amount, sum(diff) as diff
    from {sch}.zz_employment_use
    group by taz, labour_group
) sub
where diff < 0;

alter table {sch}.zz_employment_decrements add primary key (taz, labour_group);

-- Scale down employment in those combinations that have decreased.

drop table if exists {sch}.zz_employment_scaled_down_jobs;

create table {sch}.zz_employment_scaled_down_jobs as
select taz, naics, noc11, labour_group, base_amount, scale_factor,
base_amount * scale_factor as fut_jobs, base_amount * (1 - scale_factor) as lost_jobs
from
(
    select taz, naics, noc11, labour_group, coalesce(base.amt, 0) as base_amount, coalesce(loss.scale_factor, 1) as scale_factor
    from
    (
        -- Ensure we get have a value for all possible combinations so we don't have to coalesce everywhere.
        select e1.taz, e2.naics, e3.noc11, nocg.labour_group
        from (select taz from {taztbl}) e1
        cross join (select distinct naics from {emptbl}) e2
        cross join (select distinct noc11 from {emptbl}) e3
        join {noctbl} nocg using (noc11)
    ) all_combinations
    left join {emptbl} base
    using (taz, naics, noc11)
    left join {sch}.zz_employment_decrements loss
    using (taz, labour_group)
) sub;

alter table {sch}.zz_employment_scaled_down_jobs add primary key (taz, naics, noc11);

-- Add these lost jobs to the province-wide totals so they can be reallocated elsewhere.

alter table {sch}.zz_employment_jobs_by_naics_putgroup add column lost_jobs double precision;
alter table {sch}.zz_employment_jobs_by_naics_putgroup add column gross_new_jobs double precision;

update {sch}.zz_employment_jobs_by_naics_putgroup x
set lost_jobs = scaled.lost_jobs,
gross_new_jobs = greatest(x.new_jobs + scaled.lost_jobs, 0)
from
(
    select naics, labour_group, sum(lost_jobs) as lost_jobs
    from {sch}.zz_employment_scaled_down_jobs
    group by naics, labour_group
) scaled
where scaled.naics = x.naics
and scaled.labour_group = x.labour_group;

-- Allocate jobs to TAZs.

drop table if exists {sch}.zz_employment_allocate_to_tazs;

create table {sch}.zz_employment_allocate_to_tazs as

select *, case when total_diff = 0 or not increase then 0 else gross_new_jobs * diff / total_diff end as taz_jobs
from
(
    select *, sum(diff) over (partition by labour_group, naics, increase) as total_diff
    from
    (
        select taz, labour_group, naics, taz.base_amount, taz.fut_amount, taz.diff,
        coalesce(loss.diff, 0) as lost_jobs, taz.diff >= 0 and coalesce(loss.diff, 0) >= 0 as increase,
        agg.gross_new_jobs
        from
        (
            select taz, labour_group, naics as naics,
            sum(base_amount) as base_amount, sum(fut_amount) as fut_amount, sum(diff) as diff
            from {sch}.zz_employment_use
            group by taz, labour_group, naics
        ) taz
        join {sch}.zz_employment_jobs_by_naics_putgroup agg
        using (labour_group, naics)
        left join {sch}.zz_employment_decrements loss
        using (taz, labour_group)
    ) sub
) sub;

alter table {sch}.zz_employment_allocate_to_tazs add primary key (taz, labour_group, naics);

-- Allocate jobs to NOCs.

drop table if exists {sch}.zz_employment_allocate_to_nocs;

create table {sch}.zz_employment_allocate_to_nocs as
select *, base_jobs + noc_jobs - lost_jobs as fut_jobs
from
(
    select taz.taz, labour_group, naics, prop.noc11, coalesce(base.amt, 0) as base_jobs, taz.taz_jobs, prop.noc_fraction,
    taz.taz_jobs * prop.noc_fraction as noc_jobs, loss.lost_jobs
    
    --select *
    from {sch}.zz_employment_allocate_to_tazs taz
    join
    (
        -- Base year proportions
        select *, amt / sum(amt) over (partition by naics, labour_group) as noc_fraction
        from
        (
            select emp.naics, nocg.labour_group, noc11, sum(amt) as amt
            from {emptbl} emp
            join {noctbl} nocg
            using (noc11)
            group by emp.naics, nocg.labour_group, noc11
        ) sub
        --where naics = 22 and labour_group = 'L13'
    ) prop
    using (naics, labour_group)
    join {sch}.zz_employment_scaled_down_jobs loss
    using (taz, labour_group, naics, noc11)
    left join {emptbl} base
    using (taz, naics, noc11)
) sub;

alter table {sch}.zz_employment_allocate_to_nocs add primary key (taz, labour_group, naics, noc11);

-- Create output table

drop table if exists {outtbl};

create table {outtbl} as
select taz, naics, noc11, fut_jobs as amt
from {sch}.zz_employment_allocate_to_nocs
where fut_jobs > 0;