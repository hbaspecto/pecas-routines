drop table if exists {taz_skimtbl};

create table {taz_skimtbl} (
    origin_taz integer,
    destination_taz integer,
    {skim_fields},
    origin_luz integer,
    destination_luz integer,
    primary key (origin_taz, destination_taz, origin_luz, destination_luz)
);

create index on {taz_skimtbl} (origin_taz);
create index on {taz_skimtbl} (destination_taz);
create index on {taz_skimtbl} (origin_luz);
create index on {taz_skimtbl} (destination_luz);

insert into {taz_skimtbl}
select a.*, b.luz, c.luz
from {tdm_skimtbl} a
join {taztbl} b on a.origin = b.tdm_taz
join {taztbl} c on a.destination = c.tdm_taz;

-- append externals
-- External zones, use the best one for the world market
with {best_external_skim_ctes_origin}
insert into {taz_skimtbl}
select tz3o.origin,
    tz3o.destination,
    {best_external_skim_selects},
    xw.luz as o_luz, besttime.worldmarket as d_luz
from {tdm_skimtbl} tz3o
join {taztbl} xw on tz3o.origin = xw.tdm_taz
join {worldtbl} wm on tz3o.destination = wm.externalstation
{best_external_skim_joins_origin}
order by o_luz, d_luz, tz3o.origin, tz3o.destination;

with {best_external_skim_ctes_destination}
insert into {taz_skimtbl}
select tz3o.origin,
    tz3o.destination,
    {best_external_skim_selects},
    besttime.worldmarket as o_luz, xw.luz as d_luz
from {tdm_skimtbl} tz3o
join {taztbl} xw on tz3o.destination = xw.tdm_taz
join {worldtbl} wm on tz3o.origin = wm.externalstation
{best_external_skim_joins_destination}
order by o_luz, d_luz, tz3o.origin, tz3o.destination;

-- BREAK

drop table if exists {luz_skimtbl};

create table {luz_skimtbl} (
    origin integer,
    destination integer,
    {luz_skim_fields},
    primary key (origin, destination)
);

insert into {luz_skimtbl}
select origin_luz, destination_luz,
    {luz_skim_averages}
from
(
    select ts.origin_luz, ts.destination_luz,
        {luz_skim_sums}
     from {taz_skimtbl} ts, {middaytbl} f
        where ts.origin_taz = f.i and ts.destination_taz = f.j
        group by ts.origin_luz, ts.destination_luz
) sums
where origin_luz > 0
and destination_luz > 0;

{long_distance_logsum_columns}
