
import logging
import os
import subprocess
from os import path

import pecas_routines as pr


def resetdatabase(ps):
    os.environ['PGPASSWORD'] = ps.sd_password

    # Clean up results of any previous run, in particular reset the parcels to the base year parcels

    # delete the parcels file and reload from backup
    try:
        pr.reset_parcel_database(ps)
    except Exception:
        logging.error("I can't reset the SD database")
        if (ps.sd_startyear < ps.stopyear):
            raise

    # Insert inputs to database - ActivityTotals and Travel Model inputs
    if ps.load_tm_totals:
        pr.load_tm_totals(ps)
    if ps.allocate_am_totals:
        pr.load_am_totals(ps)

    if ps.calculate_import_export_size :
        pr.load_aa_totals(ps)

    # delete any old event.log file that might be lying around from previous runs.

    try:
        os.remove(ps.scendir+"/event.log")
    except OSError as detail:
        if detail.errno != 2:
            raise

def clear_mapit_outputs(ps, iface=None, years=None):
    iface = pr.get_mapit_interface(ps, iface)
    if iface is False and not ps.load_abm_land_use:
        logging.info("Not removing any results from MapIt database; load_output_to_mapit is not set so we'll leave anything that's already there in place")
        return

    if isinstance(iface, str):
        if iface.lower() == 'http':
            return clear_mapit_outputs_http(ps, years)
        elif iface.lower() == 'sql':
            return clear_mapit_outputs_sql(ps, years)

    raise TypeError("Interface {} not supported for clear_mapit_outputs().".format(iface))

def clear_mapit_outputs_http(ps, years=None):
    if ps.load_output_to_mapit:
        msg = "Removing results from scenario {0.scenario} from MapIt database {0.mapit_database} on host {0.mapit_host}".format(ps)
        if years:
            msg += " in years {}".format(years)
        logging.info(msg)
    else:
        logging.info("Not removing any results from MapIt database; load_output_to_mapit is not set so we'll leave anything that's already there in place")

    if not getattr(ps, 'mapit_httpspath', None):
        logging.error(
            "Field 'mapit_httpspath' required in pecas.yml but was not found. "
            "Cannot clear outputs on MapIt server."
        )
        return

    resp = pr.make_mapit_api_call(
        apiname       = 'clearOutputs',
        ps            = ps,
        schema        = ps.mapit_schema,
        scenario      = ps.scenario,
        removeResults = bool(ps.load_output_to_mapit),
        removeABM     = bool(ps.load_abm_land_use),
        years         = years or []
    )

    if 'fail' in resp:
        logging.error(resp['fail'])
    else:
        logging.info("\n".join(["We removed the {}".format(r) for r in resp['success']]))

def clear_mapit_outputs_sql(ps, years=None):
    os.environ['PGPASSWORD']=ps.mapit_password
    command = os.path.join(ps.pgpath, "psql")
    # remove outputs from mapit
    if ps.load_output_to_mapit:
        msg = "Removing results from scenario {0.scenario} from MapIt database {0.mapit_database} on host {0.mapit_host}".format(ps)
        if years:
            msg += " in years {}".format(years)
        logging.info(msg)

        sqlstr = "SET search_path={}; ".format(ps.mapit_schema)
        funcname = "clean_up_tables_for_scenario"
        if years:
            funcname += "_and_year"
            paramlist = ["'{}', {}".format(ps.scenario, year) for year in years]
        else:
            paramlist = ["'{}'".format(ps.scenario)]
        sqlstr += "select " + "".join(["{}({}); ".format(funcname, params) for params in paramlist])
        retcode = subprocess.check_call([command, "-c", sqlstr, "--host="+ps.mapit_host, "--port="+str(ps.mapit_port) , "--dbname="+ps.mapit_database, "--username="+ps.mapit_user])
        pr.log_results_from_external_program("We removed the results from MapIt", "Problem removing results for " + ps.scenario + " from MapIt database", (retcode,))
    else:
        logging.info("Not removing any results from MapIt database; load_output_to_mapit is not set so we'll leave anything that's already there in place")
    if ps.load_abm_land_use:
        sqlstr = "SET search_path=%s; delete from abm_se_taz10_table where scenario = '%s'; " % (ps.mapit_schema, ps.scenario)
        retcode = subprocess.check_call(
            [command, "-c", sqlstr, "--host=" + ps.mapit_host, "--port=" + str(ps.mapit_port),
             "--dbname=" + ps.mapit_database, "--username=" + ps.mapit_user])
        pr.log_results_from_external_program("We removed the ABM taz10 for the scenario", "Problem removing results for " + ps.scenario + " from ABM table", (retcode,))


if __name__ == "__main__":
    main_ps = pr.load_pecas_settings()
    pr.set_up_logging(main_ps)
    resetdatabase(main_ps)
