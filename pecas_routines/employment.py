import argparse
import logging
from os.path import join, dirname, abspath

import pecas_routines as pr
from hbautil.sqlutil import Querier
import hbautil.scriptutil as su


def main(ps, connect, curyear):
    querier = Querier(connect, debug_log=Logger())

    def schemify(tbl):
        return "{}.{}".format(ps.sd_schema, tbl)

    all_tazs_tblname = schemify("zz_all_tazs")
    base_use_tblname = schemify("zz_employment_detailed_use_base")
    base_make_tblname = schemify("zz_employment_detailed_make_base")
    cur_use_tblname = schemify("zz_employment_detailed_use_cur")
    cur_make_tblname = schemify("zz_employment_detailed_make_cur")
    emp_tblname = schemify("zz_employment_base_employment")
    work_at_home_tblname = schemify("zz_employment_work_at_home")
    noc_grp_tblname = schemify("zz_employment_noc_groups")
    put_grp_tblname = schemify("zz_employment_put_groups")
    act_naics_tblname = schemify("zz_employment_act_to_naics")
    avg_wage_tblname = schemify("zz_employment_avg_wage")
    out_tblname = schemify("zz_employment_employment")

    args = dict(
        sch=ps.sd_schema,
        taztbl=all_tazs_tblname,
        baseuse=base_use_tblname, basemake=base_make_tblname,
        curuse=cur_use_tblname, curmake=cur_make_tblname,
        emptbl=emp_tblname, wahtbl=work_at_home_tblname,
        noctbl=noc_grp_tblname, puttbl=put_grp_tblname, acttbl=act_naics_tblname, wagetbl=avg_wage_tblname,
        outtbl=out_tblname
    )

    with querier.transaction(named_tuples=True, **args) as tr:

        load_labour_file(tr, base_use_tblname, join(ps.scendir, str(ps.baseyear), "TAZDetailedUse.csv"))
        load_labour_file(tr, cur_use_tblname, join(ps.scendir, str(curyear), "TAZDetailedUse.csv"))
        tr.query("drop table if exists {taztbl}")
        if ps.employment_use_work_at_home:
            load_labour_file(tr, base_make_tblname, join(ps.scendir, str(ps.baseyear), "TAZDetailedMake.csv"))
            load_labour_file(tr, cur_make_tblname, join(ps.scendir, str(curyear), "TAZDetailedMake.csv"))

            tr.query("drop table if exists {wahtbl}")
            tr.query("create table {wahtbl} (put varchar primary key, work_at_home_fraction double precision)")
            tr.load_from_csv(work_at_home_tblname, join(ps.scendir, "AllYears", "Inputs", "WorkAtHome.csv"))

            tr.query(
                "create table {taztbl} as ("
                "select taz from {baseuse} union select taz from {curuse} "
                "union select taz from {basemake} union select taz from {curmake})"
            )
        else:
            tr.query("create table {taztbl} as (select taz from {baseuse} union select taz from {curuse})")

        tr.query("drop table if exists {noctbl}")
        tr.query("create table {noctbl} (noc11 integer primary key, labour_group varchar)")
        tr.load_from_csv(noc_grp_tblname, join(ps.scendir, "AllYears", "Inputs", "noc_groups.csv"))

        tr.query("drop table if exists {puttbl}")
        tr.query("create table {puttbl} (put varchar primary key, labour_group varchar)")
        tr.load_from_csv(put_grp_tblname, join(ps.scendir, "AllYears", "Inputs", "put_groups.csv"))

        tr.query("drop table if exists {acttbl}")
        tr.query("create table {acttbl} (activity varchar primary key, naics integer, naics_minus integer)")
        tr.load_from_csv(act_naics_tblname, join(ps.scendir, "AllYears", "Inputs", "ActivityToNaics.csv"))

        tr.query("drop table if exists {wagetbl}")
        tr.query(
            "create table {wagetbl} (\n"
            "   labour_group varchar, naics integer, average_wage double precision,\n"
            "   primary key (labour_group, naics)\n"
            ")"
        )
        tr.load_from_csv(avg_wage_tblname, join(ps.scendir, "AllYears", "Inputs", "AverageWages.csv"))

        tr.query("drop table if exists {emptbl}")
        tr.query(
            "create table {emptbl} (\n"
            "   taz integer, naics integer, noc11 integer, amt numeric,\n"
            "   primary key (taz, naics, noc11)\n"
            ")"
        )
        tr.load_from_csv(emp_tblname, join(ps.scendir, str(ps.baseyear), ps.base_employment_fname))

        allocate_employment(ps, tr)

        tr.dump_to_csv(
            "select * from {outtbl} order by taz, naics, noc11",
            join(ps.scendir, str(curyear), "Employment.csv")
        )

        # do_warnings(tr)


def load_labour_file(tr, table_name, csv_path):
    tr.query("drop table if exists {tbl}", tbl=table_name)
    tr.query(
        "create table {tbl}\n"
        "(\n"
        "   activity varchar, taz integer, put varchar, amount double precision,\n"
        "   primary key (activity, taz, put)\n"
        ")",
        tbl=table_name
    )
    tr.load_from_csv(table_name, csv_path)


def allocate_employment(ps, tr):
    sql_path = join(su.in_this_directory(__file__, "employment.sql"))
    if ps.employment_use_work_at_home:
        tr.query_external(sql_path)
    else:
        tr.query_external(sql_path, end="WAH-start")
        tr.query_external(sql_path, start="WAH-end")


def do_warnings(tr):
    whole_category_decreases = tr.query(
        "select labour_group, naics, new_jobs, lost_jobs "
        "from {sch}.zz_employment_jobs_by_naics_putgroup "
        "where new_jobs + lost_jobs < 0 "
        "order by labour_group, naics"
    )

    for labour_group, naics, new_jobs, lost_jobs in whole_category_decreases:
        logging.warning(
            "Whole category decrease in labour group {} and NAICS {}: gained {}, lost {}, gross {}"
            .format(labour_group, naics, new_jobs, lost_jobs, new_jobs + lost_jobs)
        )

    ignored_local_decreases = tr.query(
        "select taz, labour_group, naics, diff "
        "from {sch}.zz_employment_allocate_to_tazs "
        "where diff < 0 and lost_jobs >= 0 "
        "order by taz, labour_group, naics"
    )

    for taz, labour_group, naics, diff in ignored_local_decreases:
        logging.warning(
            "Ignored local decrease of {} in taz {}, labour group {}, NAICS {}"
            .format(diff, taz, labour_group, naics)
        )

    ignored_local_increases = tr.query(
        "select taz, labour_group, naics, diff, lost_jobs "
        "from {sch}.zz_employment_allocate_to_tazs "
        "where diff > 0 and lost_jobs < 0 "
        "order by taz, labour_group, naics"
    )

    for taz, labour_group, naics, diff, lost_jobs in ignored_local_increases:
        logging.warning(
            "Ignored local increase of {} in taz {}, labour group {}, NAICS {} "
            "because the whole group lost {}"
            .format(diff, taz, labour_group, naics, -lost_jobs)
        )


class Logger:
    # noinspection PyMethodMayBeStatic
    def log(self, text):
        logging.info(text)


def get_arguments_and_run():
    ps = pr.load_pecas_settings()
    pr.set_up_logging(ps)

    parser = argparse.ArgumentParser(
        description="Runs employment allocation for some years"
    )

    parser.add_argument("years", type=su.RangeList, help="years to run")
    args = parser.parse_args()
    for year in args.years.list:
        if pr.is_aa_year(ps, year):
            logging.info("Calculating employment for year {}".format(year))
            main(ps, lambda: pr.connect_to_sd(ps), year)
    logging.info("Done!")


if __name__ == "__main__":
    get_arguments_and_run()
