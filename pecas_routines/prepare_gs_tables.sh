#!/bin/bash
pushd ${CURYEAR}
sed 's/\[//g' gs_control_totals.csv | sed 's/\]//g' | grep -v 'NaN'> gs_control_totals_upl.csv
sed 's/\[//g' gs_unscaled_evs.csv | sed 's/\]//g' | grep -v 'NaN'> gs_unscaled_evs_upl.csv

sqlite3 gs.db <<EOF

-- import the data as an sqlite table.
.mode csv
.import "gs_control_totals_upl.csv" control
.schema control
.import "gs_unscaled_evs_upl.csv" unscaled
.schema unscaled

-- Create our subset.
create table gs_summary
as select 
control.TargetType, control.SpaceTypes, control.Luz,
unscaled.ExpectedValue as unscaled_ev, control.ExpectedValue as control_value
from control, unscaled 
where control.TargetType = unscaled.TargetType 
and control.SpaceTypes = unscaled.SpaceTypes
and control.Luz = unscaled.Luz ;

-- write it out
.headers on
.output gs_summary.csv
select * from gs_summary ;
.output

-- all done.
.quit

EOF

rm gs.db
popd