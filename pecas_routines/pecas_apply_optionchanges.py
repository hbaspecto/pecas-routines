#
# pecas-routines/pecas_routines/pecas_apply_optionchanges.py ---
#
# Applies the "OptionWeightChanges.csv" files to selected "TechnologyOptionsI.csv"
#
# Read the email from JEA (28-May-2021)
#
#####
#
# Reads TechnologyOptionsI.csv for that year
# For each pattern:
#    Find the rows that match the pattern
#    Multiply the weight by the weight multiplier
#
# Write out TechnologyOptionsI.csv for that year
#
# (A lot of times for things like this we load the file it
# into the PECAS database, and use SQL to change it, but
# TechnologyOptionsI is not normalized so probably best to
# avoid the database???)
#
# There is this prior art: https://bitbucket.org/hbaspecto/pecas_technology_scaling/src/234fd1d0efaa0e191e9651fcf9c46e257570929e/techscaling/update_techopt.py?at=master#lines-259
#
# Example input file OptionWeightChanges.csv:
#
# ActivityPattern,OptionNamePattern,ColumnToModify,WeightMultiplier
# HH*,*Multifamily*,OptionWeight,0.9
# HH*,*More R*,OptionWeight,1.25
#
#
# Examples in "./test_pecas_routines"
#

#####

from __future__ import (
    absolute_import,
    division,
    print_function,
    unicode_literals,
)

import argparse
import csv
import fnmatch
import os
import pdb
import pprint
import re
import subprocess
import sys
import traceback

#####


class OptionChanges(object):
    """
    Example usage:

    ::

        owc = OptionChanges(
            path="./OptionChanges.csv")

        # Process a TechnologyOptionsI.csv.
        owc.process_path(path="TechnologyOptionsI.csv")

        # made it this far, apply the changes. (do the renames)
        owc.rename_outputs()
    """

    #: What we expect in "OptionChanges"
    OWC_EXPECTED_COLUMNS = [
        'ActivityPattern',
        'OptionNamePattern',
        'ColumnToModify',
        'WeightMultiplier',  # float
    ]

    #: What we expect in the input.
    INPUT_EXPECTED_COLUMNS = [
        "Activity",
        "OptionName",
        "OptionWeight",
    ]

    def __init__(self, path, debug=None):
        self._path = None
        self._owc_rows = []
        self._to_rename_suffix = ".tmp"
        # a list of (from, to) renames to make
        self._to_rename_paths = []
        #
        self._debug = False
        if debug is not None:
            self._debug = debug
        #
        if path:
            self.read_path(path)

    def __str__(self):
        return "#<{} {:#08x}>".format(
            self.__class__.__name__,
            id(self))

    def read_path(self, path):
        """Read an optionchanges file.
        """

        self._path = path

        # grab all the rows.
        with open(self._path, "r") as fh:
            owc_reader = csv.DictReader(fh)
            self._owc_rows = list(owc_reader)
            owc_fieldnames = owc_reader.fieldnames

        # Convert to floats
        for idx, row in enumerate(self._owc_rows):
            try:
                row["WeightMultiplier"] = float(row["WeightMultiplier"])
            except (ValueError,) as e:
                raise ValueError("Cant convert OptionChanges value: {!r}: line={}".format(
                    row["WeightMultiplier"],
                    idx)) from None

        #
        if self._debug:
            pprint.pprint(self._owc_rows[0:10])

        #
        if set(owc_fieldnames) != set(self.OWC_EXPECTED_COLUMNS):
            raise ValueError("Bad headers in {!r}; Found: {!r}; Expected: {!r};".format(
                self._path,
                owc_fieldnames,
                self.OWC_EXPECTED_COLUMNS)) from None

        #
        return True

    def apply_option_weights(
            self,
            input_path=None,
            output_path=None):
        """Process an TechnologyOptions.csv file and
        write the output to the output file.
        """
        out_headers = None
        out_rows = []

        fh = open(input_path, "r")
        in_reader = csv.DictReader(fh)

        # Have expected cols?
        missing_cols = []
        for col_name in self.INPUT_EXPECTED_COLUMNS:
            if col_name not in in_reader.fieldnames:
                missing_cols.append(col_name)
        if missing_cols:
            raise ValueError("Missing columns: {!r}".format(missing_cols))

        # Apply changes.
        for in_idx, in_row in enumerate(in_reader):

            # Ooof This could be "m*n"...
            # We will try all the "owc_rows" and apply all the multipliers.
            for owc_idx, owc_row in enumerate(self._owc_rows):
                # This is simple regex, not full regex.
                # https://docs.python.org/3/library/fnmatch.html
                # fnmatch.fnmatch(value,glob_pat)
                # @TODO: regexp matches.
                if not fnmatch.fnmatch(
                        in_row["Activity"], owc_row["ActivityPattern"]):
                    continue
                if not fnmatch.fnmatch(
                        in_row["OptionName"], owc_row["OptionNamePattern"]):
                    continue
                #
                owc_columntomodify = owc_row["ColumnToModify"]
                owc_weightmultiplier = owc_row["WeightMultiplier"]
                #
                # check the column is there.
                in_value_orig = None
                try:
                    in_value_orig = float(in_row[owc_columntomodify])
                except (KeyError,) as e:
                    raise ValueError(
                        "Didnt find the {!r} column: in={} owc={}".format(
                            owc_columntomodify,
                            in_idx, owc_idx)) from None
                except (ValueError,) as e:
                    raise ValueError(
                        "Unable to convert to float: {!r}: value={!r} in={} owc={}".format(
                            owc_columntomodify,
                            in_row[owc_columntomodify],
                            in_idx, owc_idx)) from None

                #
                in_value_updated = in_value_orig * owc_weightmultiplier
                # apply the change
                in_row[owc_columntomodify] = in_value_updated
                # Matched!
                if self._debug:
                    print("### match!")
                    print("    {}".format(owc_row))
                    print(
                        "    owc_columntomodify ={}".format(owc_columntomodify))
                    print("    in_value_orig      ={}".format(in_value_orig))
                    print("    in_value_updated   ={}".format(in_value_updated))
            #
            out_rows.append(in_row)

        # write it
        output_path_tmp = output_path + ".tmp"
        with open(output_path_tmp, "w") as fh:
            out_csv = csv.DictWriter(fh, fieldnames=in_reader.fieldnames)
            out_csv.writeheader()
            for out_row in out_rows:
                out_csv.writerow(out_row)

        #
        os.rename(output_path_tmp, output_path)

        return True

    def process_path(self, path):
        """Process one path of many, delaying the rename until all are processed.

        This is handy when updating a bunch of files in-place.
        (as the renames will happen at the end.)
        """

        output_path = path + self._to_rename_suffix
        self._to_rename_paths.append((output_path, path,))
        #
        return self.apply_option_weights(
            input_path=path,
            output_path=output_path)

    def rename_outputs(self):
        print("### rename_outputs")
        for (from_path, to_path) in self._to_rename_paths:
            # print("   {} -> {}".format(from_path_tmp,to_path))
            print("   {} ".format(to_path))
            os.rename(from_path, to_path)
        print()
        return True

#####


def main(raw_args):
    global DEBUG, VERBOSE, VERSION
    #
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    parser.description = """
DESCRIPTION:

Adjusts TechnologyOptionsI.csv with OptionChanges.csv data.

"""
    parser.epilog = """
``OptionChanges.csv`` has these columns:

ActivityPattern,OptionNamePattern,ColumnToModify,WeightMultiplier

Example::

    ActivityPattern,OptionNamePattern,ColumnToModify,WeightMultiplier
    HH*,*Multifamily*,OptionWeight,0.9
    HH*,*More R*,OptionWeight,1.25

``TechnologyOptionsI.csv`` can have many columns,
but must start with these columns:

Activity,OptionName,OptionWeight

The ActivityPattern and OptionNamePattern are simple matches
(glob-style) for the Activity, OptionName colums and when
matched, will adjust the value in the ColumnToModify by
multipling it by the "WeightMultiplier".

If mutiple rows in OptionChanges.csv match,
then mutiple adjustments will be made; one for each row.

The glob-style matches are:
  ? = matches one char
  * = matches a bunch of chars

See "https://docs.python.org/3/library/fnmatch.html"
for the matching done.

EXAMPLES:

  single file mode:
    pecas-apply-optionchanges \\
      --optionchanges-csv 2035/OptionChanges.csv \\
      --input-path 2025/TechnologyOptionsIn.csv \\
      --output-path 2025/TechnologyOptionsOut.csv

  multi change mode:

    pecas-apply-optionchanges \\
      --optionchanges-csv 2035/OptionChanges.csv \\
      */TechnologyOptionsI.csv

"""
    #
    g = parser.add_argument_group('GENERAL')
    g.add_argument("--debug", "-d",
                   action="store_true",
                   help="Turn on the debugging flag.")
    g.add_argument("--pdb",
                   action="store_true",
                   help=argparse.SUPPRESS)
    g.add_argument("--verbose", "-v",
                   action="store_true",
                   help="Be more verbose.")

    #
    g = parser.add_argument_group('PECAS APPLY OPTIONWEIGHTCHANGES')

    g.add_argument("--regexp", "-E",
                   action="store_true",
                   help="Use python regex, instead of the default glob when matching. (@TODO!)")

    g.add_argument("--optionchanges-csv", "--changes",
                   help="Path to the OptionChanges.csv to apply.")

    g.add_argument("--input-file",
                   help="path to input file. (single-mode)")

    g.add_argument("--output-file",
                   help="path to output file. (single-mode)")

    g.add_argument("args", nargs="*",
                   help="The list of TechnologyOptionsI.csv files to process. (multi-mode)")

    #
    args = parser.parse_args(raw_args)

    #
    if args.pdb:
        pdb.set_trace()
    if args.debug:
        DEBUG = args.debug
    if args.verbose:
        VERBOSE = args.verbose

    if not args.optionchanges_csv:
        print("### --optionweightchanges-csv is required.")
        return 1

    try:
        #
        owc = OptionChanges(
            path=args.optionchanges_csv,
            debug=args.debug)

        # single-mode = one file.
        if args.input_file or args.output_file:
            # check the args.
            if args.input_file is None:
                print("--input-file required when --output-file is used.")
                return 1
            if args.output_file is None:
                print("--output-file required when --input-file is used.")
                return 1
            #
            owc.apply_option_weights(
                input_path=args.input_file,
                output_path=args.output_file)
            return 0

        # multi-mode = Process the paths
        for path in args.args:
            owc.process_path(path=path)
        owc.rename_outputs()
        return 0

    except (Exception,) as e:
        if args.debug:
            traceback.print_exc()
        print("ERROR: ", e)
        return -1

#####


def run_default():
    return main_entry()


def main_entry():
    sys.exit(main(sys.argv[1:]))


if __name__ == "__main__":
    main_entry()
