import psycopg2 as pg


import pecas_routines as pr
from pecas_routines.migrate import skip


def main(ps):
    querier = pr.sd_querier(ps)
    with querier:
        create_taz_groups(querier)
        create_tazs_by_taz_group(querier)
        create_taz_group_constants(querier)
        create_taz_group_space_constants(querier)
        create_taz_limit_groups(querier)
        create_taz_limit_space_types(querier)
        create_space_taz_limits(querier)
        create_space_taz_limits_view(querier)
        create_random_seeds(querier)


def create_taz_groups(q):
    with q.transaction() as tr:
        try:
            stmt = ("create table {sch}.taz_groups "
                    "(taz_group_id integer not null, "
                    "taz_group_name character varying, "
                    "constraint taz_groups_pkey primary key (taz_group_id)"
                    ")")

            tr.query(stmt)

            print("Created table taz_groups")
        except pg.ProgrammingError as e:
            skip(e, "taz_groups")


def create_tazs_by_taz_group(q):
    with q.transaction() as tr:
        try:
            stmt = ("create table {sch}.tazs_by_taz_group "
                    "(taz_number integer not null, "
                    "taz_group_id integer, "
                    "constraint tazs_by_taz_group_pkey primary key (taz_number), "
                    "constraint taz_fkey foreign key (taz_number) "
                    "references {sch}.tazs (taz_number) "
                    "match simple on update cascade on delete cascade, "
                    "constraint taz_group_fkey foreign key (taz_group_id) "
                    "references {sch}.taz_groups (taz_group_id) "
                    "match simple on update cascade on delete cascade"
                    ")")

            tr.query(stmt)

            print("Created table tazs_by_taz_group")
        except pg.ProgrammingError as e:
            skip(e, "tazs_by_taz_group")


def create_taz_group_constants(q):
    with q.transaction() as tr:
        try:
            stmt = ("create table {sch}.taz_group_constants\n"
                    "(\n"
                    "taz_group_id integer primary key,\n"
                    "construction_constant double precision,\n"
                    "foreign key (taz_group_id) references {sch}.taz_groups (taz_group_id)\n"
                    ")")

            tr.query(stmt)

            print("Created table taz_group_constants")
        except pg.ProgrammingError as e:
            skip(e, "taz_group_constants")


def create_taz_group_space_constants(q):
    with q.transaction() as tr:
        try:
            stmt = ("create table {sch}.taz_group_space_constants\n"
                    "(\n"
                    "taz_group_id integer,\n"
                    "space_type_id integer,\n"
                    "construction_constant double precision,\n"
                    "primary key (taz_group_id, space_type_id),\n"
                    "foreign key (taz_group_id) references {sch}.taz_groups (taz_group_id),\n"
                    "foreign key (space_type_id) references {sch}.space_types_i (space_type_id)\n"
                    ")")

            tr.query(stmt)

            print("Created table taz_group_space_constants")
        except pg.ProgrammingError as e:
            skip(e, "taz_group_space_constants")


def create_taz_limit_groups(q):
    with q.transaction() as tr:
        try:
            stmt = ("create table {sch}.taz_limit_groups "
                    "(taz_limit_group_id integer not null, "
                    "taz_limit_group_name character varying(150), "
                    "constraint taz_limit_groups_pkey primary key "
                    "(taz_limit_group_id)"
                    ")")

            tr.query(stmt)

            print("Created table taz_limit_groups")
        except pg.ProgrammingError as e:
            skip(e, "taz_limits_group")


def create_taz_limit_space_types(q):
    with q.transaction() as tr:
        try:
            stmt = ("create table {sch}.taz_limit_space_types "
                    "(space_type_id integer not null, "
                    "taz_limit_group_id integer not null, "
                    "constraint taz_limit_space_types_pkey primary key "
                    "(space_type_id), "
                    "constraint taz_limit_space_type_fkey foreign key "
                    "(space_type_id) "
                    "references {sch}.space_types_i (space_type_id) "
                    "match simple on update cascade on delete cascade, "
                    "constraint taz_limit_space_types_group_fkey foreign key "
                    "(taz_limit_group_id) "
                    "references {sch}.taz_limit_groups "
                    "(taz_limit_group_id) match simple "
                    "on update cascade on delete cascade"
                    ")")

            tr.query(stmt)

            print("Created table taz_limit_space_types")
        except pg.ProgrammingError as e:
            skip(e, "taz_limit_space_types")


def create_space_taz_limits(q):
    with q.transaction() as tr:
        try:
            stmt = ("create table {sch}.space_taz_limits "
                    "(taz_group_id integer not null, "
                    "taz_limit_group_id integer not null, "
                    "min_quantity double precision, "
                    "max_quantity double precision, "
                    "year_effective integer not null, "
                    "manual boolean, "
                    "constraint space_taz_limits_pkey primary key "
                    "(taz_group_id, taz_limit_group_id, year_effective), "
                    "constraint space_taz_limits_taz_fkey foreign key (taz_group_id) "
                    "references {sch}.taz_groups (taz_group_id) match simple "
                    "on update no action on delete no action, "
                    "constraint space_taz_limits_group_fkey foreign key "
                    "(taz_limit_group_id) "
                    "references {sch}.taz_limit_groups "
                    "(taz_limit_group_id) match simple "
                    "on update cascade on delete cascade"
                    ")")

            tr.query(stmt)

            print("Created table space_taz_limits")
        except pg.ProgrammingError as e:
            skip(e, "space_taz_limits")


def create_space_taz_limits_view(q):
    with q.transaction() as tr:
        stmt = ("create or replace view {sch}.most_recent_taz_limit_year as "
                "select space_taz_limits.taz_group_id, "
                "max(space_taz_limits.year_effective) "
                "as current_limit_year "
                "from {sch}.space_taz_limits "
                "join {sch}.current_year_table "
                "on space_taz_limits.year_effective "
                "<= current_year_table.current_year "
                "group by space_taz_limits.taz_group_id")

        tr.query(stmt)

        print("Created view most_recent_taz_limit_year")


def create_random_seeds(q):
    with q.transaction() as tr:
        try:
            stmt = (
                "create table {sch}.random_seeds\n"
                "(\n"
                "   pecas_parcel_num bigint,\n"
                "   year_effective int,\n"
                "   seed bigint,\n"
                "   primary key (pecas_parcel_num, year_effective),\n"
                "   foreign key (pecas_parcel_num) references {sch}.parcels (pecas_parcel_num)\n"
                ")\n"
            )

            tr.query(stmt)

            print("Created table random_seeds")
        except pg.ProgrammingError as e:
            skip(e, "random_seeds")


def run_default():
    main_ps = pr.load_pecas_settings()
    main(main_ps)


if __name__ == "__main__":
    run_default()
