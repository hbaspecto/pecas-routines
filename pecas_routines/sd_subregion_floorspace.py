import logging
from os.path import join

import pecas_routines as pr
from hbautil import scriptutil as su


def main(ps):
    querier = pr.sd_querier(ps)
    with querier.transaction() as tr:
        tr.query(
            "create table {sch}.zy_floorspace_sd_base\n"
            "(\n"
            "   year_run integer,\n"
            "   taz integer,\n"
            "   commodity varchar,\n"
            "   quantity double precision,\n"
            "   primary key (year_run, taz, commodity)\n"
            ");"
        )

        tr.query(
            "alter view {sch}.floorspacei_view\n"
            "rename to floorspacei_view_raw;"
        )

        tr.query(
            "create view {sch}.floorspacei_view as\n"
            "select coalesce(fraw.\"TAZ\", fsd.taz) as \"TAZ\",\n"
            "coalesce(fraw.\"Commodity\", fsd.commodity) as \"Commodity\",\n"
            "coalesce(fraw.\"Quantity\", fsd.quantity) as \"Quantity\""
            "from {sch}.floorspacei_view_raw fraw\n"
            "full join\n"
            "(\n"
            "   {sch}.current_year_table cy\n"
            "   join\n"
            "   {sch}.zy_floorspace_sd_base fsd\n"
            "   on fsd.year_run = cy.current_year\n"
            ")\n"
            "on fsd.taz = fraw.\"TAZ\"\n"
            "and fsd.commodity = fraw.\"Commodity\"\n"
        )

        tr.query(
            "create table {sch}.floorspace_sd_base_temp\n"
            "(\n"
            "   taz integer,\n"
            "   commodity varchar,\n"
            "   quantity double precision\n"
            ");"
        )

        for year in su.irange(ps.baseyear, ps.endyear):
            try:
                tr.load_from_csv(
                    "{sch}.floorspace_sd_base_temp",
                    join(ps.scendir, str(year), "FloorspaceSD.csv")
                )
            except IOError:
                logging.warning("No FloorspaceSD file in year {}".format(year))
            else:
                logging.info("Copying FloorspaceSD in year {}".format(year))
                tr.query(
                    "insert into {sch}.zy_floorspace_sd_base\n"
                    "select {yr}, taz, commodity, quantity\n"
                    "from {sch}.floorspace_sd_base_temp",
                    yr=year
                )
                tr.query(
                    "truncate table {sch}.floorspace_sd_base_temp;"
                )
        tr.query("drop table {sch}.floorspace_sd_base_temp")
