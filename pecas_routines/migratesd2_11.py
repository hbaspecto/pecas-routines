import textwrap

import pecas_routines as pr
from pecas_routines.migrate import run_skipping


def main(ps):
    querier = pr.sd_querier(ps)
    with querier:
        create_local_effects_view(querier)
        create_redevelopment_control(querier)


def create_local_effects_view(q):
    def fun(tr):
        tr.query(textwrap.dedent(
            """
            create materialized view {sch}.local_effect_distances_most_recent
            as
            SELECT distinct on (pecas_parcel_num, local_effect_id)
                pecas_parcel_num, local_effect_id, local_effect_distance, year_effective
                FROM {sch}.local_effect_distances,
                {sch}.current_year_table
                where year_effective <= current_year
                order by pecas_parcel_num, local_effect_id, year_effective desc;
                
            CREATE INDEX lef_index_pn
                ON {sch}.local_effect_distances_most_recent
                (pecas_parcel_num, local_effect_id);
            """
        ))
    run_skipping(q, "local_effect_distances_most_recent", fun)


def create_redevelopment_control(q):
    def fun(tr):
        tr.query(textwrap.dedent(
            """
            create table {sch}.redevelopment_control
            (
                space_type_id integer primary key,
                max_addition_intensity double precision,
                min_demolition_age double precision,
                constraint redevelopment_control_fkey foreign key (space_type_id)
                references {sch}.space_types_i (space_type_id)
                match simple on update cascade on delete cascade
            )
            """
        ))
    run_skipping(q, "redevelopment_control", fun)


def run_default():
    main_ps = pr.load_pecas_settings()
    main(main_ps)


if __name__ == "__main__":
    run_default()
