import argparse

import pecas_routines as pr
from pecas_routines import sd_subregion_floorspace as srf, sd_subregion_prices as srp


def main(ps, luzs):
    luzs = tuple(luzs)
    querier = pr.sd_querier(ps)
    with querier.transaction(luzs=luzs) as tr:
        tr.query(
            "delete from {sch}.sitespec_parcels ss\n"
            "using {sch}.parcels par, {sch}.tazs\n"
            "where par.pecas_parcel_num = ss.pecas_parcel_num\n"
            "and tazs.taz_number = par.taz\n"
            "and tazs.luz_number not in %(luzs)s;"
        )

        tr.query(
            "delete from {sch}.parcels par\n"
            "using {sch}.tazs\n"
            "where tazs.taz_number = par.taz\n"
            "and tazs.luz_number not in %(luzs)s;"
        )

        tr.query(
            "delete from {sch}.parcels_backup par\n"
            "using {sch}.tazs\n"
            "where tazs.taz_number = par.taz\n"
            "and tazs.luz_number not in %(luzs)s;"
        )

        tr.query(
            "delete from {sch}.parcels_backup_with_geom par\n"
            "using {sch}.tazs\n"
            "where tazs.taz_number = par.taz\n"
            "and tazs.luz_number not in %(luzs)s;"
        )


def run_default():
    parser = argparse.ArgumentParser(
        description="Run this script on a PECAS database to convert it to an SD subregion database, "
                    "where SD only runs in a specific subregion defined by LUZs."
    )

    parser.add_argument(
        "luz_file",
        help="The file containing the list of LUZs in the subregion, separated by commas and/or newlines"
    )
    parser.add_argument(
        "-f", "--floorspace", action="store_true",
        help="Whether to load the FloorspaceSD file for each year into the database as a base for the subregion "
             "run's floorspacei_view; use this if you want to run the subregion SD together with a full-region AA"
    )
    parser.add_argument(
        "-p", "--prices", action="store_true",
        help="Whether to filter the ExchangeResults file for each year to only contain the subregion zones; "
             "use this if you want to run the subregion SD alone using prices from a full-region AA"
    )

    args = parser.parse_args()
    with open(args.luz_file) as luz_file:
        luzs = [int(luz.strip()) for row in luz_file for luz in row.split(",")]

    ps = pr.load_pecas_settings()
    main(ps, luzs)
    if args.floorspace:
        srf.main(ps)
    if args.prices:
        srp.main(ps, luzs)


if __name__ == "__main__":
    run_default()
