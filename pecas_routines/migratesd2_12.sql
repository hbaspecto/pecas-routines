begin;

CREATE SEQUENCE i268.zoning_rules_code_seq;

SELECT setval('i268.zoning_rules_code_seq',(
	SELECT GREATEST(MAX(zoning_rules_code)+1,nextval('i268.zoning_rules_code_seq'))-1 
	FROM i268.zoning_rules_i));

ALTER TABLE i268.zoning_rules_i ALTER COLUMN zoning_rules_code
        SET DEFAULT nextval('i268.zoning_rules_code_seq'::regclass);

commit;

begin; 

ALTER TABLE i268.zoning_permissions
    ADD COLUMN maximum_development_size double precision;

COMMENT ON COLUMN i268.zoning_permissions.maximum_development_size
    IS 'The maximum size of development on any parcel.';
	
ALTER TABLE i268.zoning_permissions
    ADD COLUMN minimum_development_size double precision;

COMMENT ON COLUMN i268.zoning_permissions.minimum_development_size
    IS 'The minimum size of development on any parcel.';
	
commit;

-- EG for edmonton

update table i267.zoning_permissions set maximum_development_size = 3000 where space_type_id in (23,33)
and zoning