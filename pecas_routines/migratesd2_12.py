import textwrap

import pecas_routines as pr
from pecas_routines.migrate import run_skipping


def main(ps):
    querier = pr.sd_querier(ps)
    with querier:
        create_zoning_code_sequence(querier)
        add_min_and_max_dev_size(querier)


def create_zoning_code_sequence(q):
    def fun(tr):
        tr.query(textwrap.dedent(
            """
            CREATE SEQUENCE {sch}.zoning_rules_code_seq;
            
            SELECT setval('{sch}.zoning_rules_code_seq',(
                SELECT GREATEST(MAX(zoning_rules_code)+1,nextval('{sch}.zoning_rules_code_seq'))-1 
                FROM {sch}.zoning_rules_i));
            
            ALTER TABLE {sch}.zoning_rules_i ALTER COLUMN zoning_rules_code
                    SET DEFAULT nextval('{sch}.zoning_rules_code_seq'::regclass);
            """
        ))
    run_skipping(q, "create_zoning_code_sequence", fun)


def add_min_and_max_dev_size(q):
    def fun(tr):
        tr.query(textwrap.dedent(
            """
            ALTER TABLE {sch}.zoning_permissions
                ADD COLUMN maximum_development_size double precision default null;
            
            COMMENT ON COLUMN {sch}.zoning_permissions.maximum_development_size
                IS 'The maximum size of development on any parcel.';
                
            ALTER TABLE {sch}.zoning_permissions
                ADD COLUMN minimum_development_size double precision default null;
            
            COMMENT ON COLUMN {sch}.zoning_permissions.minimum_development_size
                IS 'The minimum size of development on any parcel.';
                
            """
        ))
    run_skipping(q, "add_min_and_max_dev_size", fun)


def run_default():
    main_ps = pr.load_pecas_settings()
    main(main_ps)


if __name__ == "__main__":
    run_default()
