from setuptools import setup

setup(
    name="pecas_routines",
    version="1.0",
    packages=["pecas_routines"],
    package_data={
        "": [
            "*.exe",
            "*.sh",
            "*.sql",
            ],
    },
    install_requires=[
        "numpy",
        "openmatrix",
        # psycopg2-2.9 changed "copy_from", so we cant pass in a schema as part of our tablename.
        # Pin it to 2.8.6 while we do code changes.
        # see also hba-util/setup.py
        "psycopg2-binary",
        "setuptools-git",
        "pyyaml",
    ],
    entry_points={
        "console_scripts": [
            "migratesd2_9 = pecas_routines.migratesd2_9:run_default",
            "migratesd2_10 = pecas_routines.migratesd2_10:run_default",
            "migratesd2_11 = pecas_routines.migratesd2_11:run_default",
            "migratesd2_12 = pecas_routines.migratesd2_12:run_default",
            "sd_subregion = pecas_routines.sd_subregion:run_default",
            "pecas-apply-optionchanges = pecas_routines.pecas_apply_optionchanges:run_default",
            "squeeze-skims = pecas_routines.skims_to_sem:run_default",
        ]
    }
)
