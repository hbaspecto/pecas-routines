I want to extract a subset of the San Diego skims that's independent of the
rest of the skims.

So we need to choose a few LUZs, then extract all the TAZs in those LUZs,
plus any TAPs that map to those TAZs.

But we also need to consider transit access. We don't want transit users
to be forced to use a farther TAP because the closest one disappeared.



I used this query on the T26 database to find LUZs whose closest access
points were all still within the LUZ:


    with access_origin_luz as
    (
        select access_taz.origin, origin_taz.luz as origin_luz, access_taz.origin_station, access_taz.station_taz, station_taz.luz as station_luz from
        (
            select origin, origin_station, station_taz, access_skim from 
            (
                select ac.origin, st.station as origin_station, st.taz as station_taz, ac.access_skim, min(ac.access_skim) over (partition by ac.origin) as shortest_access
                from t26.zz_transit_access_access_skims ac
                join t26.zz_transit_access_stations st
                on st.taz = ac.destination
            ) oa
            where access_skim = shortest_access
        ) access_taz
        join t26.zz_skim_squeeze_transit_tdm_zones origin_taz on origin_taz.tdm_taz = access_taz.origin
        join t26.zz_skim_squeeze_transit_tdm_zones station_taz on station_taz.tdm_taz = access_taz.station_taz
    ),

    origin_luz_same as
    (
        select origin_luz, same_luz, count(*) from
        (
            select *, origin_luz = station_luz as same_luz from access_origin_luz
        ) sub
        group by origin_luz, same_luz
    )

    select origin_luz
    from origin_luz_same o1
    left join (select * from origin_luz_same where not same_luz) o2 using (origin_luz)
    where o2.origin_luz is null
    order by origin_luz;


The candidate LUZs:

    1
    5
    12
    15
    28
    38
    44
    45
    53
    58
    89
    90
    91
    99
    107
    111
    117
    123
    124
    129
    131
    132
    181
    217
    219
    222
    223
    225
    229

Then the same thing with egress:

    with access_destination_luz as
    (
        select access_taz.destination, destination_taz.luz as destination_luz, access_taz.destination_station, access_taz.station_taz, station_taz.luz as station_luz from
        (
            select destination, destination_station, station_taz, access_skim from 
            (
                select ac.destination, st.station as destination_station, st.taz as station_taz, ac.access_skim, min(ac.access_skim) over (partition by ac.destination) as shortest_access
                from t26.zz_transit_access_access_skims ac
                join t26.zz_transit_access_stations st
                on st.taz = ac.origin
            ) oa
            where access_skim = shortest_access
        ) access_taz
        join t26.zz_skim_squeeze_transit_tdm_zones destination_taz on destination_taz.tdm_taz = access_taz.destination
        join t26.zz_skim_squeeze_transit_tdm_zones station_taz on station_taz.tdm_taz = access_taz.station_taz
    ),

    destination_luz_same as
    (
        select destination_luz, same_luz, count(*) from
        (
            select *, destination_luz = station_luz as same_luz from access_destination_luz
        ) sub
        group by destination_luz, same_luz
    )

    select destination_luz
    from destination_luz_same o1
    left join (select * from destination_luz_same where not same_luz) o2 using (destination_luz)
    where o2.destination_luz is null
    order by destination_luz;


This produces these candidates:
    5
    12
    15
    28
    38
    44
    45
    53
    58
    89
    90
    91
    99
    107
    111
    117
    123
    124
    129
    131
    132
    181
    217
    219
    222
    223
    225
    229


This is the same list, without zone 1. So just pick a few of these. Let's
go with 5, 99, and 181.

We also need the externals: LUZs 230-236, TAZs 2, 6, 10, and 12.

For the externals, we need the closest TAP for each
external zone so that transit trips to those zones don't change. This is the access query:

    select * from
    (
        select origin, origin_station, station_taz, access_skim from 
        (
            select ac.origin, st.station as origin_station, st.taz as station_taz, ac.access_skim, min(ac.access_skim) over (partition by ac.origin) as shortest_access
            from t26.zz_transit_access_access_skims ac
            join t26.zz_transit_access_stations st
            on st.taz = ac.destination
        ) oa
        where access_skim = shortest_access
    ) access_taz
    where origin in (2, 6, 10, 12)
    
Results: TAP 7, 34, 1156, and 2477.

And the egress query:

    select * from
    (
        select destination, destination_station, station_taz, access_skim from 
        (
            select ac.destination, st.station as destination_station, st.taz as station_taz, ac.access_skim, min(ac.access_skim) over (partition by ac.destination) as shortest_access
            from t26.zz_transit_access_access_skims ac
            join t26.zz_transit_access_stations st
            on st.taz = ac.origin
        ) oa
        where access_skim = shortest_access
    ) access_taz
    where destination in (2, 6, 10, 12)

Results are the same as for access.

This means we also need TAZs 16, 150, 2472, and 4973 in TapToTaz.

But we also won't assign these zones to any LUZs, so they
won't influence the internal trips.
