import extract_test as et

from os.path import join

target_folder = "F:/PECASGeneral/pecas-routines/test/skims_to_sem/San Diego"

with open(join(target_folder, "test_tazs.txt")) as f:
    tazs = [int(taz.strip()) for taz in f]

class Tazs:
    list = tazs

class TazsPlus:
    list = tazs + [16, 150, 2472, 4973]

with open(join(target_folder, "test_taps.txt")) as f:
    taps = [int(tap.strip()) for tap in f]

class Taps:
    list = taps

class Luzs:
    list = [5, 99, 181] + list(range(230, 237))
    

print("Extracting station numbers")
et.main("AllYears/Inputs/StationNumbers.csv", join(target_folder, "StationNumbers.csv"), taz=Taps)

print("Extracting station to TAZ mapping")
et.main("AllYears/Inputs/TapToTaz.csv", join(target_folder, "TapToTaz.csv"), station=Taps)

print("Extracting midday flows")
et.main("AllYears/Inputs/MiddayFlows.csv", join(target_folder, "MiddayFlows.csv"), i=Tazs, j=Tazs)

print("Extracting AM traffic skims")
et.main("2016/traffic_skims_AM.omx", join(target_folder, "2016", "TestTrafficSkimsAM.omx"), zone_number=TazsPlus)

print("Extracting MD traffic skims")
et.main("2016/traffic_skims_MD.omx", join(target_folder, "2016", "TestTrafficSkimsMD.omx"), zone_number=TazsPlus)

print("Extracting transit skims")
et.main("2016/transit_skims.omx", join(target_folder, "2016", "TestTransitSkims.omx"), zone_number=Taps)

print("Extracting expected results")
et.main("2016/SkimsI.csv", join(target_folder, "ExpectedSkims.csv"), origin=Luzs, destination=Luzs)
