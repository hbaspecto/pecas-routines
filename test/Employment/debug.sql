-- Vs TazUse

select sum(diff) from
(
    select taz, labour_group, naics_minus as naics,
    sum(base_amount) as base_amount, sum(fut_amount) as fut_amount, sum(diff) as diff
    from test.zz_employment_use_diff
    group by taz, labour_group, naics_minus
    order by taz, labour_group, naics_minus
) sub

-- Good!


-- Vs AggregatedUse

select sum(new_jobs) from
(
    select * from test.zz_employment_jobs_by_naics_putgroup
    order by labour_group, naics
) sub

-- Good up to new_jobs (the further columns are calculated later)


-- Vs Decrements

select taz, labour_group, base_amount, diff, 1 + diff / base_amount as scale_factor
from
(
    select taz, labour_group, sum(base_amount) as base_amount, sum(diff) as diff
    from test.zz_employment_use_diff
    group by taz, labour_group
) sub
where diff < 0
order by taz, labour_group

-- Good!


-- Vs Employment

select sum(lost_jobs) from
(
    select * from test.zz_employment_scaled_down_jobs
    order by taz, naics, noc11
) sub

-- Good!


-- Vs the last two columns of AggregateUse

select sum(gross_new_jobs) from
(
    select * from test.zz_employment_jobs_by_naics_putgroup
    order by labour_group, naics
) sub

-- Good!


-- Vs the later columns TazUse

select sum(taz_jobs) from
(
    select * from test.zz_employment_allocate_to_tazs
    order by taz, labour_group, naics
) sub

-- Good!


-- Vs BaseNocPortions

select avg(noc_fraction) from
(
    select *, amt / sum(amt) over (partition by naics, labour_group) as noc_fraction
    from
    (
        select emp.naics, nocg.labour_group, noc11, sum(amt) as amt
        from test.zz_employment_base_employment emp
        join test.zz_employment_noc_groups nocg
        using (noc11)
        group by emp.naics, nocg.labour_group, noc11
    ) sub
    order by naics, noc11, labour_group
) sub

-- Good!


-- Vs SplitToNocs

select * from test.zz_employment_allocate_to_nocs
order by taz, labour_group, naics, noc11

-- Vs later columns in Employment
select * from test.zz_employment_allocate_to_nocs
order by taz, naics, noc11

-- Final result
select * from test.zz_employment_employment
order by taz, naics, noc11