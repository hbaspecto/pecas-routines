select * from output.all_taz_detailed_use where scenario = 'I181' and year_run = 2011 limit 1

select distinct taz from output.all_taz_detailed_use where scenario = 'I181' and year_run = 2011 order by taz


-- Look for zones where the activity amount has decreased
with use as (
    select year_run, taz, commodity, -sum(amount) as amount
    from output.all_taz_detailed_use
    where scenario = 'I181'
    group by year_run, taz, commodity
)

select taz, commodity, u11.amount as amount11, u39.amount as amount39
from use u11
join use u39
using (taz, commodity)
where u11.year_run = 2011 and u39.year_run = 2039
order by taz, commodity;

-- Some examples:
-- 10102 has L161718 going down
-- 10203 has L15 going down
-- Maybe that's enough to work with? So we pick out the first eight internal zones individually,
-- then group everything else by first digit.

select activity as "Activity", taz as "TAZ", commodity as "Commodity", sum(amount) as "Amount"
from (
    select activity,
    case when taz between 10101 and 10203 then taz
    else taz / 100000 end
    as taz,
    commodity, -amount as amount
    from output.all_taz_detailed_use
    where scenario = 'I181'
--     and year_run = 2011
    and year_run = 2039
    and taz >= 10000
) sub
group by activity, taz, commodity
order by activity, taz, commodity

-- The make isn't in MapIt, so I manually loaded it into a database and did the same query as above.
